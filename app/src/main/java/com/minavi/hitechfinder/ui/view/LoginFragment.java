package com.minavi.hitechfinder.ui.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.minavi.hitechfinder.ui.R;
import com.minavi.hitechfinder.ui.base.BaseFragment;
import com.minavi.hitechfinder.ui.view.model.LoginDTO;
import com.minavi.hitechfinder.ui.view.model.ServiceResponseDTO;
import com.minavi.hitechfinder.ui.data.remote.net.request.HTFController;
import com.minavi.hitechfinder.ui.presenters.LoginPresenter;
import com.minavi.hitechfinder.ui.view.callbacks.ILoginView;

import javax.inject.Inject;

/**
 * Created by yugandhar on 6/10/2017.
 */

public class LoginFragment extends BaseFragment implements ILoginView{

//add change
    @Inject
    LoginPresenter loginPresenter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        fragmentComponent().inject(this);
        loginPresenter.attachView(this);
        final View rootView=inflater.inflate(R.layout.layout_login,container,false);

        Button loginButton= (Button) rootView.findViewById(R.id.btnLogin);
        TextView forgotText= (TextView) rootView.findViewById(R.id.forgot_password);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginDTO loginDTO=new LoginDTO();
                String email=((EditText)rootView.findViewById(R.id.et_username)).getText().toString();
                String password=((EditText)rootView.findViewById(R.id.et_password)).getText().toString();
                if(email.isEmpty())
                    ((EditText)rootView.findViewById(R.id.et_username)).setError("UserName manditory");
                else if(password.isEmpty())
                    ((EditText)rootView.findViewById(R.id.et_password)).setError("Pssword manditory");
                else
                    {
                    loginDTO.setEmail(email);
                    loginDTO.setPassword(password);
                    HTFController htfController = new HTFController(getActivity());
                    htfController.logInAPICall(LoginFragment.this, loginDTO);
                }
            }
        });
        forgotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForGotView(getActivity());
            }
        });

     return rootView;
    }

    @Override
    public void handleLoginResponce(ServiceResponseDTO serviceResponseDTO)
    {
                Toast.makeText(getActivity(),serviceResponseDTO.getStatusMsg(),Toast.LENGTH_LONG).show();
                Intent intent=new Intent(getActivity(),MainActivity.class);
                getActivity().finish();
                startActivity(intent);
    }

    @Override
    public void handleForgetResponce(ServiceResponseDTO serviceResponseDTO)
    {
         Toast.makeText(getActivity(),serviceResponseDTO.getStatusMsg(),Toast.LENGTH_LONG).show();
    }

    @Override
    public void showError(int iStatusCode, String sMessage) {

    }

    private void showForGotView(Context mContext)
    {


        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (getActivity()).getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_forget_password, null);

        final EditText emailText= (EditText) dialoglayout.findViewById(R.id.forget_email);


        Button button= (Button) dialoglayout.findViewById(R.id.btn_get_password);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emailText.getText().toString().trim().isEmpty())
                {
                    emailText.setError("Email manditory");
                }
                else
                    {
                    HTFController htfController = new HTFController(getActivity());
                    htfController.ForgotAPICall(LoginFragment.this, emailText.getText().toString().trim());
                }
            }
        });

        builder.setTitle("Forgot Password");
        builder.setCancelable(false);
//        builder.setIcon(R.drawable.galleryalart);
        builder.setView(dialoglayout)
                // Add action buttons
                .setPositiveButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id)
                    {
                        dialog.dismiss();
                    }

    });
    builder.create();
    builder.show();

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        loginPresenter.detachView();
    }
}
