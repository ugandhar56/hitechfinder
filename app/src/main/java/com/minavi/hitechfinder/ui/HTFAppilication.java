package com.minavi.hitechfinder.ui;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.minavi.hitechfinder.ui.injection.component.ApplicationComponent;
import com.minavi.hitechfinder.ui.injection.component.DaggerApplicationComponent;
import com.minavi.hitechfinder.ui.injection.module.ApplicationModule;

import io.realm.Realm;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by yugandhar on 6/10/2017.
 */

public class HTFAppilication extends Application {

    static ApplicationComponent mApplicationComponent;
    private static  HTFAppilication clInstance=null;
    private RequestQueue mRequestQueue;

    @Override
    public void onCreate()
    {
        super.onCreate();

      /*  if (BuildConfig.DEBUG)
        {
            Timber.plant(new Timber.DebugTree());
        }
        else*/
        {

        }        HTFAppilication.clInstance=this;

        Realm.init(this);
    }

    public static HTFAppilication get(Context context) {
        return (HTFAppilication) context.getApplicationContext();
    }

    public static ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(HTFAppilication.clInstance))
                    .build();
        }

        return mApplicationComponent;
    }
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
