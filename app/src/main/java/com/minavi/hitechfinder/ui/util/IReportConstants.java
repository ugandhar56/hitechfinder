package com.minavi.hitechfinder.ui.util;

/**
 * Created by J.yugandhar on 27-02-2017.
 */
public interface IReportConstants {
    interface IAlign
    {

        public static final byte HLEFT=1 ;
        public static final byte HCENTER=2;
        public static final byte HRIGHT=3;


        public static final byte VTOP=1;
        public static final byte VMIDDLE=2;
        public static final byte VBOTTOM=3;

    }
    interface IOrder
    {
        public static final byte ASCENDING=0;
        public static final byte DESCENDING=1;
    }

    interface IRowType
    {
        public static final byte EMPTY_ROW= -1;
        public static final byte NORMAL=0;

        public static final byte GRP_TITLE_STYLE1=2; // VALUE_AS_TITLE only
        public static final byte GRP_TITLE_STYLE2=3; // VALUE_AS_TITLE_COUNT
        public static final byte GRP_TITLE_STYLE3=4; // COLUMN NAME _VALUE_AS_TITLE
        public static final byte GRP_TITLE_STYLE4=5; // COLUMN NAME_VALUE_AS_TITLE_COUNT

        public static final byte ROWS_COUNT=6;
        public static final byte PAGE_TOTAL =7;/// from 5 to 20 r resserved
        public static final byte BLANK_LINE=8;
        public static final byte PRINT_UNDER_NEW_ROW = 9;
        public static final byte GRAND_TOTAL = 10;


        public static final byte GRP_TOTAL_LEVEL1=21;
        public static final byte GRP_TOTAL_LEVEL2=22;
        public static final byte GRP_TOTAL_LEVEL3=23;
        public static final byte GRP_TOTAL_LEVEL4=24;
        public static final byte GRP_TOTAL_LEVEL5=25;
        public static final byte GRP_TOTAL_LEVEL6=26;
    }
    interface IRowGroupStyle
    {
        // bit reserved for 1
        public static final byte USER_ACTION=-1;  // user hits show/hide button at render time...
        public static final byte SHOW_DETAILS=0;
        public static final byte HIDE_DETAILS=1;

        public static final byte NONE=0;
        // bit reserved for 1
        public static final byte STYLE_LEFT=1;
        public static final byte STYLE_CENTER=2;
        public static final byte STYLE_PARTICULARS=3;

        public static final byte STYLE_SUMMARY=4;// STYLE_PARTICULARS+ only summary values of the groupsi,e only numberic fields+ Hide details
        public static final byte STYLE_SUMMARY_NO_GROUPS=5;// present groups as columns and  summary values of the groupsi,e only numberic fields+ Hide details

    }
}
