package com.minavi.hitechfinder.ui.data.remote.net.provider;


import com.minavi.hitechfinder.ui.data.remote.net.dto.CLRequestDTO;
import com.minavi.hitechfinder.ui.data.remote.net.dto.CLResponseDTO;

/**
 * Created by yugandhar on 10-11-2016.
 */
public interface IHttpProvider
{
    public CLResponseDTO executeRequest(CLRequestDTO clRequestDTO);


}
