package com.minavi.hitechfinder.ui.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.minavi.hitechfinder.ui.R;
import com.minavi.hitechfinder.ui.view.model.RegisterDTO;
import com.minavi.hitechfinder.ui.view.model.ServiceResponseDTO;
import com.minavi.hitechfinder.ui.data.remote.net.request.HTFController;
import com.minavi.hitechfinder.ui.view.callbacks.ISignUpView;

/**
 * Created by yugandhar on 6/10/2017.
 */

public class SignUpFragment extends Fragment implements ISignUpView,View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.layout_signup,container,false);
        Button sinupbutton= (Button) rootView.findViewById(R.id.btn_Signup);
        sinupbutton.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId()==R.id.btn_Signup)
        {

            RegisterDTO clRegisterDTO=new RegisterDTO();

            String sEmail=((EditText)getActivity().findViewById(R.id.et_signup_email)).getText().toString().trim();
            String sUserName=((EditText)getActivity().findViewById(R.id.et_signup_username)).getText().toString().trim();
            String sPassword=((EditText)getActivity().findViewById(R.id.et_signup_password)).getText().toString().trim();
            String sRePassword=((EditText)getActivity().findViewById(R.id.et_signup_repassword)).getText().toString().trim();

            if(sEmail.isEmpty())
                ((EditText)getActivity().findViewById(R.id.et_signup_email)).setError("Email Manditory");
            else if(sUserName.isEmpty())
                ((EditText)getActivity().findViewById(R.id.et_signup_username)).setError("UserName Manditory");
            else if(sPassword.isEmpty())
                ((EditText)getActivity().findViewById(R.id.et_signup_password)).setError("Password Manditory");
            else if(sRePassword.isEmpty())
                ((EditText)getActivity().findViewById(R.id.et_signup_repassword)).setError("Re Password Manditory");
            else if(!sPassword.equals(sRePassword))
                Toast.makeText(getActivity(),"confirm password and password shold be same",Toast.LENGTH_LONG).show();
            else {
                clRegisterDTO.setEmail(((EditText) getActivity().findViewById(R.id.et_signup_email)).getText().toString().trim());
                clRegisterDTO.setUserName(((EditText) getActivity().findViewById(R.id.et_signup_username)).getText().toString().trim());
                clRegisterDTO.setPassword(((EditText) getActivity().findViewById(R.id.et_signup_password)).getText().toString().trim());
                clRegisterDTO.setRePassword(((EditText) getActivity().findViewById(R.id.et_signup_repassword)).getText().toString().trim());

                HTFController htfController = new HTFController(getActivity());
                htfController.SignUPAPICall(SignUpFragment.this, clRegisterDTO);
            }

        }


    }
    @Override
    public void handleSignUpResponce(ServiceResponseDTO serviceResponseDTO)
    {
        Toast.makeText(getActivity(),serviceResponseDTO.getStatusMsg(),Toast.LENGTH_LONG).show();
        ViewPager viewPager= (ViewPager) getActivity().findViewById(R.id.login_viewpager);
        viewPager.setCurrentItem(0);

    }
}
