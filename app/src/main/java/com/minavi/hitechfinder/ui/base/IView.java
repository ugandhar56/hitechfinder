package com.minavi.hitechfinder.ui.base;


/**
 * Base interface that any class that wants to act as a View in the MVP (Model View IPresenter)
 * pattern must implement. Generally this interface will be extended by a more specific interface
 * that then usually will be implemented by an Activity or Fragment.
 */
public interface IView
{

    public void showError(int iStatusCode, String sMessage);

}
