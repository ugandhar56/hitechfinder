package com.minavi.hitechfinder.ui.view.model;

import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

import java.io.Serializable;

/**
 * Created by nagababu on 27-12-2016.
 */
public class UserDTO extends RealmObject implements Serializable
{

    @PrimaryKey
    byte byRealmPk=1;

    @SerializedName("user_id")
    private int iLoginId;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("profile_picture")
    private String sImageName;

    @SerializedName("email")
    private  String sEmail;
    @SerializedName("gender")
    private String sGender;
    @SerializedName("dob")
    private String dob;

    public int getLoginId() {
        return iLoginId;
    }

    public void setLoginId(int iLoginId) {
        this.iLoginId = iLoginId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageName() {
        return sImageName;
    }

    public void setImageName(String sImageName) {
        this.sImageName = sImageName;
    }

    public String getEmail() {
        return sEmail;
    }

    public void setEmail(String sEmail) {
        this.sEmail = sEmail;
    }

    public String getGender() {
        return sGender;
    }

    public void setGender(String sGender) {
        this.sGender = sGender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
}
