package com.minavi.hitechfinder.ui.view.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by yugandhar on 12/3/2016.
 */

public class InstitutesDTO extends RealmObject implements Serializable {


    @SerializedName("id")
    @PrimaryKey
    private int id;
    @SerializedName("institute_name")
    @Expose
    private String institute_name;
    @SerializedName("institute_logo")
    @Expose
    private String institute_logo;
    @SerializedName("established_on")
    @Expose
    private String established_on;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("website")
    @Expose
    private  String website;
    @SerializedName("address")
    @Expose
    private String  address;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("category")
    private String category;

    public void setInstitute_name(String institute_name) {
        this.institute_name = institute_name;
    }

    public void setInstitute_logo(String institute_logo) {
        this.institute_logo = institute_logo;
    }

    public void setEstablished_on(String established_on) {
        this.established_on = established_on;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getInstitute_name() {
        return institute_name;
    }

    public String getInstitute_logo() {
        return institute_logo;
    }

    public String getEstablished_on() {
        return established_on;
    }

    public String getEmail() {
        return email;
    }

    public String getMobile() {
        return mobile;
    }

    public String getWebsite() {
        return website;
    }

    public String getAddress() {
        return address;
    }

    public String getRating() {
        return rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    /* final GsonBuilder gsonBuilder = new GsonBuilder();
    final Gson gson = gsonBuilder.create();*/
   // Type listType = new TypeToken<List<InstitutesDTO>>() {}.getType();
//    List<InstitutesDTO> list = gson.fromJson(new InputStreamReader(inputStream), listType);
}
