package com.minavi.hitechfinder.ui.data.remote.net.request;


import com.minavi.hitechfinder.ui.data.remote.net.dto.CLRequestDTO;
import com.minavi.hitechfinder.ui.data.remote.net.dto.CLResponseDTO;

/**
 * Created by yugandhar on 10-11-2016.
 */
public interface IRequest
{
    public static final byte CONTENT_FORM=1;
    public static final byte CONTENT_JSON=2;
    public static final byte CONTENT_MULTIPART=3;




    public static final byte METHOD_GET=1;
    public static final byte METHOD_POST=2;


//    public void sendRequest(CLRequestDTO clRequestDTO);

    public CLResponseDTO sendRequest(CLRequestDTO clRequestDTO) throws RuntimeException;

}
