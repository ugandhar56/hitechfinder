package com.minavi.hitechfinder.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.minavi.hitechfinder.ui.R;
import com.minavi.hitechfinder.ui.data.remote.net.request.HTFController;
import com.minavi.hitechfinder.ui.view.callbacks.IChangeView;
import com.minavi.hitechfinder.ui.view.callbacks.ISignUpView;
import com.minavi.hitechfinder.ui.view.model.RegisterDTO;
import com.minavi.hitechfinder.ui.view.model.ServiceResponseDTO;

/**
 * Created by yugandhar on 7/2/2017.
 */

public class ChangePassword extends AppCompatActivity implements IChangeView{

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar;
        actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.layout_change_password);
        Button changeButton= (Button)findViewById(R.id.btn_change_password);

        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sEmail=((EditText)findViewById(R.id.et_change_email)).getText().toString().toString();
                String spassword=((EditText)findViewById(R.id.et_change_password)).getText().toString().toString();
                String srePassword=((EditText)findViewById(R.id.et_change_repassword)).getText().toString().toString();

                if(sEmail.isEmpty())
                    ((EditText) findViewById(R.id.et_change_email)).setError("Emial Manditroy");
                else if(spassword.isEmpty())
                    ((EditText)findViewById(R.id.et_change_password)).setError("Password Manditroy");
                else if(srePassword.isEmpty())
                    ((EditText) findViewById(R.id.et_change_repassword)).setError("Re Password Manditroy");
                else if(spassword.equals(srePassword))
                    Toast.makeText(ChangePassword.this,"Both Passwords should not be same",Toast.LENGTH_LONG).show();
                else
                {
                    RegisterDTO registerDTO=new RegisterDTO();
                    registerDTO.setEmail(sEmail);
                    registerDTO.setPassword(spassword);
                    registerDTO.setRePassword(srePassword);
                    HTFController htfController=new HTFController(ChangePassword.this);
                    htfController.ChangeAPICall(ChangePassword.this,registerDTO);

                }
            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void handleChangePassword(ServiceResponseDTO serviceResponseDTO)
    {
        Toast.makeText(ChangePassword.this,serviceResponseDTO.getStatusMsg(),Toast.LENGTH_LONG).show();
        Intent intent=new Intent(ChangePassword.this,LoginActivity.class);
        startActivity(intent);

    }

/*
    public class ChangeDTO
   {
       String email;
       String password;
       String newPassword;

       public String getEmail() {
           return email;
       }

       public void setEmail(String email) {
           this.email = email;
       }

       public String getPassword() {
           return password;
       }

       public void setPassword(String password) {
           this.password = password;
       }

       public String getRePassword() {
           return newPassword;
       }

       public void setRePassword(String rePassword) {
           this.newPassword = rePassword;
       }
   }
*/
}
