package com.minavi.hitechfinder.ui.injection.module;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by srikanth on 03/11/2016.
 */

@Module
public class FragmentModule
{

    private Fragment mFragment;

    public FragmentModule(Fragment clFragment) {
        mFragment = clFragment;
    }

    @Provides
    Fragment provideFragment() {
        return mFragment;
    }

    @Provides    
    Activity providesActivity() {
        return mFragment.getActivity();
    }

}
