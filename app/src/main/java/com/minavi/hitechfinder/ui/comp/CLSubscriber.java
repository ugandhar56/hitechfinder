package com.minavi.hitechfinder.ui.comp;

import android.view.ViewGroup;

import com.minavi.hitechfinder.ui.HTFAppilication;
import com.minavi.hitechfinder.ui.base.IView;
import com.minavi.hitechfinder.ui.view.model.ServiceResponseDTO;

public abstract class CLSubscriber<T extends ServiceResponseDTO> extends rx.Subscriber<T>
{

    IView clView=null;
    ViewGroup clViewGroup=null;
    protected CLSubscriber(IView clView)
    {
        super(null, false);
        this.clView=clView;
        final ProgressHandler clProgressHandler= ProgressHandler.getInstance();
        clProgressHandler.show(clView, ProgressHandler.SHOW_ON_SCREEN);

    }

    /**
     * This is used to attach a progress bar for every viewgroup and detach on resposnse from the view group, use this to show multiple progress bar on differrent views at the same time.
     * @param clView
     * @param clViewGroup
     */
    protected CLSubscriber(IView clView, ViewGroup clViewGroup)
    {
        super(null, false);
        this.clView=clView;
        this.clViewGroup=clViewGroup;
        final ProgressHandler clProgressHandler= ProgressHandler.getInstance();
        clProgressHandler.attachToViewGroup(HTFAppilication.getComponent().context(),clViewGroup);

    }



    /**
     * Construct a CLSubscriber by using another CLSubscriber for backpressure and
     * for holding the subscription list (when <code>this.add(sub)</code> is
     * called this will in fact call <code>subscriber.add(sub)</code>).
     *
     * @param subscriber
     *            the other CLSubscriber
     */
    public CLSubscriber(IView clView, rx.Subscriber<?> subscriber)
    {
        super(subscriber, true);
        final ProgressHandler clProgressHandler= ProgressHandler.getInstance();
        clProgressHandler.show(clView, ProgressHandler.SHOW_ON_SCREEN);
    }



    @Override
    public   void onCompleted()
    {
        if(clViewGroup!=null)
        {
            final ProgressHandler clProgressHandler= ProgressHandler.getInstance();
            clProgressHandler.detachFromViewGroup(HTFAppilication.getComponent().context(),clViewGroup);

        }
       else
        {
            final ProgressHandler clProgressHandler = ProgressHandler.getInstance();
            clProgressHandler.hide();
        }



    }

    @Override
    public  void onError(Throwable e)
    {
        if(clViewGroup!=null)
        {
            final ProgressHandler clProgressHandler= ProgressHandler.getInstance();
            clProgressHandler.detachFromViewGroup(HTFAppilication.getComponent().context(),clViewGroup);
        }
        else {
            final ProgressHandler clProgressHandler = ProgressHandler.getInstance();
            clProgressHandler.hide();
        }
        clView.showError(2, e.getMessage());
    }





}
