package com.minavi.hitechfinder.ui.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.focus.centra.uicontrols.view.list.recycler.listener.IItemViewHolderListener;
import com.focus.centra.uicontrols.view.list.recycler.view.CLItemViewHolder;
import com.focus.centra.uicontrols.view.list.recycler.view.CLRecyclerView;
import com.minavi.hitechfinder.ui.R;
import com.minavi.hitechfinder.ui.data.local.realm.CLRealmHelper;
import com.minavi.hitechfinder.ui.data.remote.net.request.HTFController;
import com.minavi.hitechfinder.ui.view.callbacks.InstutionsView;
import com.minavi.hitechfinder.ui.view.model.InstitutesDTO;
import com.minavi.hitechfinder.ui.view.model.ServiceResponseDTO;
import com.minavi.hitechfinder.ui.view.model.UserDTO;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,IItemViewHolderListener,InstutionsView
{
    ArrayList<InstitutesDTO> arrInstList=null;
    CLRecyclerView clRecyclerView=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        arrInstList=new ArrayList<>();
        arrInstList.add(new InstitutesDTO());


        clRecyclerView=new CLRecyclerView(this,arrInstList,this);
        clRecyclerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.isSmoothScrollbarEnabled();
        clRecyclerView.setLayoutManager(linearLayoutManager);
//        navigationView.addView(clRecyclerView);
        LinearLayout constraintLayout= (LinearLayout) findViewById(R.id.list);
        constraintLayout.addView(clRecyclerView);
        UserDTO userDTO= CLRealmHelper.getInstance().getUserDetails();

        HTFController htfController=new HTFController(this);
        htfController.getInstutes(this);
        View header = navigationView.getHeaderView(0);
//        TextView text = (TextView) header.findViewById(R.id.textView);
        ((TextView)header.findViewById(R.id.user_name)).setText(userDTO.getFirstName());
        ((TextView)header.findViewById(R.id.user_email)).setText(userDTO.getEmail());


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout)
        {
            // Handle the camera action
            HTFController htfController=new HTFController(this);
            htfController.Logout();
        } else if (id == R.id.nav_change_password)
        {

            Intent intent=new Intent(MainActivity.this,ChangePassword.class);
            startActivity(intent);

        } else if (id == R.id.nav_myinstutes) {

        } else if (id == R.id.nav_myprofile) {

        } else if (id == R.id.nav_myrevies) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public CLItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        LayoutInflater inflater = LayoutInflater.from(this);
        LinearLayout clView = (LinearLayout) LinearLayout.inflate(MainActivity.this, R.layout.institute_list_item_view, null);
        clView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        return new ViewHolder(clView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
    {

        if(viewHolder instanceof ViewHolder) {
            InstitutesDTO clContactDTO = (InstitutesDTO) arrInstList.get(position);
            if(clContactDTO.getInstitute_name()!=null)
            ((ViewHolder)viewHolder).instName.setText(clContactDTO.getInstitute_name());
            if(clContactDTO.getAddress()!=null)
            ((ViewHolder)viewHolder).instAddress.setText(clContactDTO.getAddress());
            if(clContactDTO.getCategory()!=null)
            ((ViewHolder)viewHolder).instCategory.setText(clContactDTO.getCategory());
            if(clContactDTO.getRating()!=null&&!clContactDTO.getRating().equals(""))
            ((ViewHolder)viewHolder).instRating.setRating(Float.valueOf(clContactDTO.getRating()));
        }

    }

    @Override
    public int getItemViewType(int i) {
        return 0;
    }

    @Override
    public void handleInstutesResponce(ServiceResponseDTO serviceResponseDTO)
    {
        arrInstList= (ArrayList<InstitutesDTO>) serviceResponseDTO.getResponse();
        clRecyclerView.setArrayList(arrInstList);
        clRecyclerView.getAdapter().notifyDataSetChanged();
    }

    class ViewHolder extends CLItemViewHolder
    {
        public TextView instName;
        public TextView instAddress;
        public TextView instCategory;
        public RatingBar instRating;

        public ViewHolder(View itemView)
        {
            super(itemView,null,null);
            this.instName = (TextView) itemView.findViewById(R.id.rv_inst_name);
            this.instAddress = (TextView) itemView.findViewById(R.id.rv_inst_address);
            this.instCategory = (TextView) itemView.findViewById(R.id.rv_inst_category);
            this.instRating = (RatingBar) itemView.findViewById(R.id.rv_inst_rating);

        }
    }

    private ArrayList<InstitutesDTO> getInstList()
    {
        String[] instNames=new String []{"sathya","Naresh","Durga"};
        String[]categoryes=new String[]{"FrontEnd","Backend","Mobile"};
        String[]address=new String[]{"Satyam theater","Balkampet Road","Mithrivam building"};
        String[]ratings=new String[]{"4.0","5.0","3.5"};
        ArrayList<InstitutesDTO> arrListDtos=new ArrayList<>();

        for(int i=0;i<instNames.length;i++)
        {
            InstitutesDTO institutesDTO=new InstitutesDTO();
            institutesDTO.setInstitute_name(instNames[i]);
            institutesDTO.setAddress(address[i]);
            institutesDTO.setCategory(categoryes[i]);
            institutesDTO.setRating(ratings[i]);
            arrListDtos.add(institutesDTO);

        }
        return arrListDtos;
    }
/**/
}
