package com.minavi.hitechfinder.ui.data.remote.service;


import com.minavi.hitechfinder.ui.view.model.LoginDTO;

import rx.Observable;

/**
 * Created by yugandhar on 6/13/2017.
 */

public interface ILoginService  {

    public Observable authenticateUser(LoginDTO loginDTO);
}
