package com.minavi.hitechfinder.ui.base;

import org.json.JSONException;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by j.yugandhar on 06-10-2017.
 */
public class BaseObservable<T> implements Observable.OnSubscribe<T> ,IBaseObservable<T>
{

    @Override
    public final void call(Subscriber<? super T> subscriber)
    {
        if (!subscriber.isUnsubscribed())
        { //2
            try
            {
                callSubscriber(subscriber);
            }
            catch (Exception e)
            {
                subscriber.onError(e);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void callSubscriber(Subscriber subscriber) throws JSONException {

    }
}
