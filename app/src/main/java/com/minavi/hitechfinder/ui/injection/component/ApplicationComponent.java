package com.minavi.hitechfinder.ui.injection.component;

import android.app.Application;
import android.content.Context;

import com.minavi.hitechfinder.ui.injection.ApplicationContext;
import com.minavi.hitechfinder.ui.injection.module.ApplicationModule;

import javax.inject.Singleton;


import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    //void inject();



    @ApplicationContext
    Context context();
    Application application();

    /*RibotsService ribotsService();
    PreferencesHelper preferencesHelper();
    DatabaseHelper databaseHelper();
    DataManager dataManager();
    RxEventBus eventBus();*/

//    CLDataManager dataManager();






}
