package com.minavi.hitechfinder.ui.data.remote.net;

/**
 * Created by srikanth on 26/12/2016.
 */
public class NetException extends  RuntimeException
{
    int iErrorId=0;
    public NetException(String sMessage, int iErrorId)
    {
        super(sMessage);
        this.iErrorId=iErrorId;
    }
    public NetException(Exception clExcepion, int iErrorId)
    {
        super(clExcepion);
        this.iErrorId=iErrorId;
    }
}
