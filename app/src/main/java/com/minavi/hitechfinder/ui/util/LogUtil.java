package com.minavi.hitechfinder.ui.util;

import timber.log.Timber;

/**
 * Created by J.yugandhar on 04-11-2016.
 */
public class LogUtil {

    static String SOURCE="Cenrta CRM";

    public static void setSource(String sSource){
        LogUtil.SOURCE=sSource;
    }

    public static void d(Throwable t, String message, Object... args)
    {

        String clCallerClass=Thread.currentThread().getStackTrace()[3].getFileName();
        Timber.tag(clCallerClass);
        Timber.d(t,message,args);
//        Timber.d(message);
    }

    public static void d( String message, Object... args)
    {

        String clCallerClass=Thread.currentThread().getStackTrace()[3].getFileName();
        Timber.tag(clCallerClass);
        Timber.d(message,args);
//        Timber.d(msg);
    }

    public static void w(String message, Object... args)
    {
        String clCallerClass=Thread.currentThread().getStackTrace()[3].getFileName();
        Timber.tag(clCallerClass);
        Timber.i(message, args);
    }



    public static void e(String sMessage, Object... args){

        /*String clCallerClass=Thread.currentThread().getStackTrace()[3].getFileName();
        Timber.tag(clCallerClass);
        Timber.e(message, args);*/
        e(null,sMessage,args);
    }

    public static void e(Throwable t,  String sMessage)
    {
        e(t,  sMessage, null);
    }

    public static void e(Throwable t,  String sMessage, Object... args)
    {
        String clCallerClass=Thread.currentThread().getStackTrace()[3].getFileName();
        Timber.tag(clCallerClass);
        Timber.e(t,sMessage, args);
    }

    public static void i(String message, Object... args){
       /* StackTraceElement trace=Thread.currentThread().getStackTrace()[3];
        String clCallerClass=trace.getFileName()+":"+trace.getMethodName()+":"+trace.getLineNumber();
        Timber.tag(clCallerClass);
       *//* Throwable stack = new Throwable().fillInStackTrace();
        StackTraceElement[] trace = stack.getStackTrace();
        String clCallerClass=trace[1].getFileName();
          String logfrom= trace[1].getClassName() + "." + trace[1].getMethodName() + ":" + trace[1].getLineNumber();*//*
        Timber.tag(clCallerClass);*/
//        Reflection.getCallingClass();
        String clCallerClass=Thread.currentThread().getStackTrace()[3].getFileName();
        Timber.tag(clCallerClass);
        Timber.i(message,args);
    }

    public static void v(String message, Object... args)
    {
        String clCallerClass=Thread.currentThread().getStackTrace()[3].getFileName();
        Timber.tag(clCallerClass);
        Timber.v(message,args);
    }
}

