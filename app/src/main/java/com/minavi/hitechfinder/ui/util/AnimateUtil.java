package com.minavi.hitechfinder.ui.util;

import android.view.View;
import android.view.animation.Animation;
import android.widget.LinearLayout;

/**
 * Created by srikanth on 21/11/2016.
 */
public class AnimateUtil {


    /*
* Expands given view with animation
*/
    public static void expand(final View view)
    {
        view.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = view.getMeasuredHeight();
        view.getLayoutParams().height = 0;
        view.setVisibility(View.VISIBLE);
        Animation anim = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, android.view.animation.Transformation t)
            {
                view.getLayoutParams().height = (interpolatedTime == 1) ? LinearLayout.LayoutParams.WRAP_CONTENT : (int)(targetHeight * interpolatedTime);
                view.requestLayout();
            }
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        anim.setDuration((int)(targetHeight / view.getContext().getResources().getDisplayMetrics().density));// 1dp/ms
//		  anim.setInterpolator(new AccelerateInterpolator(0.5f));
        view.startAnimation(anim);
    }

    /*
    * Collapses given view with animation
    */
    public static void collapse(final View view)
    {
        final int initialHeight = view.getMeasuredHeight();
        Animation anim = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, android.view.animation.Transformation t) {
                if(interpolatedTime == 1)
                    view.setVisibility(View.GONE);
                else{
                    view.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    view.requestLayout();
                }
            }
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        anim.setDuration((int)(initialHeight / view.getContext().getResources().getDisplayMetrics().density));// 1dp/ms
//		  anim.setInterpolator(new AccelerateInterpolator(0.5f));
        view.startAnimation(anim);
    }
}
