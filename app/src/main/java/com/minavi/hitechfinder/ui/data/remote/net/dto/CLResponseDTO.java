package com.minavi.hitechfinder.ui.data.remote.net.dto;

/**
 * Created by n.satish on 25-11-2016,13:05.
 */
public class CLResponseDTO<T> {

    byte byStatus=0;
    T objResponse;

    public byte getStatus() {
        return byStatus;
    }

    public void setStatus(byte byStatus) {
        this.byStatus = byStatus;
    }

    public T getResponse() {
        return objResponse;
    }

    public void setResponse(T objResponse) {
        this.objResponse = objResponse;
    }
}
