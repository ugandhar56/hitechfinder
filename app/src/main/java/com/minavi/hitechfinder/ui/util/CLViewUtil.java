package com.minavi.hitechfinder.ui.util;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.provider.Settings;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.focus.centra.uicontrols.util.CLUIUtil;
import com.minavi.hitechfinder.ui.R;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;

public final class CLViewUtil {

    public static int getAndroidVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static float pxToDp(float px) {
        float densityDpi = Resources.getSystem().getDisplayMetrics().densityDpi;
        return px / ((float) densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static int dpToPx(int dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm =
                (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
    }

    public static void showKeyboard(Activity activity, View v) {
        if (activity != null)
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        else if (v != null) {
            try {
                InputMethodManager imManager = (InputMethodManager) v.getContext().getSystemService(Service.INPUT_METHOD_SERVICE);
                imManager.showSoftInputFromInputMethod(v.getWindowToken(), 0);
                //imManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public static boolean isMobileDevice(Context context) {
        return !
                ((context.getResources().getConfiguration().screenLayout
                        & Configuration.SCREENLAYOUT_SIZE_MASK)
                        >= Configuration.SCREENLAYOUT_SIZE_LARGE);
    }


    /*
    * Returns device's current orientation
    * */
    public static int getCurrentOrientation(Context clContext) {
        int iOrientation = clContext.getResources().getConfiguration().orientation;

        if (iOrientation == Configuration.ORIENTATION_UNDEFINED) {
            Display clDisplay = ((Activity) clContext).getWindowManager().getDefaultDisplay();
            int iWidth, iHeight;

            //			int sdk = android.os.Build.VERSION.SDK_INT;
            //				Point size = new Point();
            iWidth = clDisplay.getWidth();
            iHeight = clDisplay.getHeight();

            if (iWidth == iHeight)
                iOrientation = Configuration.ORIENTATION_SQUARE;

            if (iWidth < iHeight)
                iOrientation = Configuration.ORIENTATION_PORTRAIT;
        }

        return iOrientation;//Configuration.ORIENTATION_LANDSCAPE; // Portrait Mode
    }


    /*
    * Returns width of device
    */
    public static int getDeviceWidth(Context context) {

//		System.out.println("Device width is:"+((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth());
        return ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth();
    }

    /*
    * Returns height of device
    */
    public static int getDeviceHeight(Context context) {
        return ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getHeight();
    }


  /*  *//*
    * Finishes given activity and shows animation
	*//*
    public static void finishActivity(Activity activity)
    {
        if(activity != null)
        {
            activity.finish();
            activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    *//*
    * Starts given activity and shows animation
    *//*
    public static void startActivity(Activity activity, Intent intent)
    {
        if(activity != null && intent != null)
        {
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }*/


    /*
    * Converts given string to bitmap
    */
    public static Bitmap StringToBitmap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    /*
    * Converts given bytes to Base64 string
    */
    public static String BytesToString(byte[] bytes) {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    /*
    * Converts given bitmap to string
    */
    public static String BitmapToString(Bitmap bitmap) {
        byte[] bytes = BitmapToBytes(bitmap);
        return BytesToString(bytes);
    }

    /*
    * Converts given bitmap to bytes
    */
    public static byte[] BitmapToBytes(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    /*
    * Converts given bitmap to bytes
    */
    public static byte[] BitmapToBytes(Bitmap bitmap, Bitmap.CompressFormat format) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(format, 100, baos);
        return baos.toByteArray();
    }


    /*
    * Returns unique device id
    */
    public static String getUniqueDeviceId(Context clContext) {
        String sReturn = null;
        sReturn = getAndroidId(clContext);

        if (sReturn != null && sReturn.trim().length() > 0) {
            if (CLUIUtil.isNumeric(sReturn) && Integer.parseInt(sReturn) > 0)
                return sReturn;
            else
                return sReturn;
        } else {
            sReturn = getIMEINumber(clContext);
            if (sReturn != null && sReturn.trim().length() > 0) {
                if (CLUIUtil.isNumeric(sReturn) && Integer.parseInt(sReturn) > 0)
                    return sReturn;
                else
                    return sReturn;
            } else {
                sReturn = getSerialNo(clContext);
                if (sReturn != null && sReturn.trim().length() > 0) {
                    if (CLUIUtil.isNumeric(sReturn) && Integer.parseInt(sReturn) > 0)
                        return sReturn;
                    else
                        return sReturn;
                }
            }
        }
        return sReturn;
    }

    /*
    * Returns device IMEI number
    */
    public static String getIMEINumber(Context clContext) {
        TelephonyManager telephonyManager = (TelephonyManager) clContext.getSystemService(Context.TELEPHONY_SERVICE);
        String sIMEI = telephonyManager.getDeviceId();
        return sIMEI;
    }

    /*
    * Returns android id
    */
    public static String getAndroidId(Context clContext) {
        return Settings.Secure.getString(clContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    /*
    * Returns device serial number
    */
    public static String getSerialNo(Context clContext) {
        String serialnum = null;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class, String.class);
            serialnum = (String) (get.invoke(c, "ro.serialno", ""));

        } catch (Exception ignored) {
        }

        if (serialnum == null || serialnum.trim().length() == 0) {
            try {
                Class<?> myclass = Class.forName("android.os.SystemProperties");
                Method[] methods = myclass.getMethods();
                Object[] params = new Object[]{new String("ro.serialno"), new String("")};
                serialnum = (String) (methods[2].invoke(myclass, params));

            } catch (Exception ignored) {
            }
        }

        return serialnum;
    }


    public static int getActionBarHeight(Context context) {
        TypedValue tv = new TypedValue();
        context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true);
        return context.getResources().getDimensionPixelSize(tv.resourceId);
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void setImageURL(Context context, String sURL, final ImageView clImageView) {
    /*    GlideUrl glideUrl = new GlideUrl(getURL(context, sURL), new LazyHeaders.Builder()
                .addHeader("Cookie", CLAppBuffer.getInstance().getUserDTO().getSessionId())
                .build());
*/
        Glide.with(context)
                .load(sURL)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(clImageView);

    }

    public static void setImageOnline(Context context, String sURL, final ImageView clImageView, boolean isLoad) {
        Glide.with(context)
                .load(sURL)

                .placeholder(R.drawable.ic_action_done)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(clImageView);
        if (isLoad) {
            CustomGlide customGlide = new CustomGlide();
            customGlide.registerComponents(context, Glide.get(context));
        }

    }




    public static StateListDrawable setSelectorColorState(int color, int iDefaultColor, int iState) {
        StateListDrawable res = new StateListDrawable();
//        res.setAlpha(45);
        res.addState(new int[]{iState/*android.R.attr.state_pressed*/}, new ColorDrawable(color));
        res.addState(new int[]{}, new ColorDrawable(iDefaultColor));
        return res;
    }

    public static StateListDrawable getSelectorDrawable(int color, int iDefaultColor, int iExitFadeDuration) {
        StateListDrawable res = new StateListDrawable();
        res.setExitFadeDuration(iExitFadeDuration);
//        res.setAlpha(45);
        res.addState(new int[]{android.R.attr.state_pressed}, new ColorDrawable(color));
        res.addState(new int[]{}, new ColorDrawable(iDefaultColor));
        return res;
    }


    public static Drawable getDrawableCustom(Context context, int iDrawable, int iColor) {
        return getDrawableCustom(context.getResources().getDrawable(iDrawable), iColor);
    }

    public static Drawable getDrawableCustom(Drawable drawable, int icolor) {
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable);
        /*if(icolor<=0)
            icolor=R.color.colorText2;*/
            DrawableCompat.setTint(drawable, icolor);
        }
//        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.OVERLAY);
        return drawable;
    }

    public static Drawable getIcon(Context context, int iIconId) {
        int iDrawable = context.getResources().getIdentifier(String.valueOf("ic_" + iIconId), "drawable", context.getPackageName());
        Drawable drawable = null;
        if (iDrawable > 1) {
            drawable = context.getResources().getDrawable(iDrawable);
            drawable = DrawableCompat.wrap(drawable);
        /*if(icolor<=0)
            icolor=R.color.colorText2;*/
            DrawableCompat.setTint(drawable, CLThemeUtil.getThemeAssetColor(context));
        }
        return drawable;
    }


}
