package com.minavi.hitechfinder.ui.data.local.realm.model;

import io.realm.RealmObject;

/**
 * Created by J.yugandhar on 23-01-2017.
 */
public class CLRealmInt extends RealmObject {
    private int val;

    public CLRealmInt() {
    }

    public CLRealmInt(int val) {
        this.val = val;
    }

}
