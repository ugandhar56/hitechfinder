package com.minavi.hitechfinder.ui.data.remote.net.request;


import com.minavi.hitechfinder.ui.data.remote.net.dto.CLRequestDTO;
import com.minavi.hitechfinder.ui.data.remote.net.dto.CLResponseDTO;
import com.minavi.hitechfinder.ui.data.remote.net.provider.IHttpProvider;
import com.minavi.hitechfinder.ui.data.remote.net.provider.OkHttpProvider;

/**
 * Created by yugandhar on 10-11-2016.
 */
public abstract class CLRequest implements IRequest//,IResponse.Listener
{
    //protected IResponse.Listener clResponseListener=null;

    /*public void sendRequest(CLRequestDTO clRequestDTO)
    {
        try
        {
            IHttpProvider clHttpProvider=new CLOkHttpProvider();
            clHttpProvider.sendRequest(clRequestDTO,null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }*/

    @Override
    public CLResponseDTO sendRequest(CLRequestDTO clRequestDTO)
    {
        CLResponseDTO clResponseDTO=null;

        //this.clResponseListener=clResponseListener;
        IHttpProvider clHttpProvider=new OkHttpProvider();
        clResponseDTO=clHttpProvider.executeRequest(clRequestDTO);

       // String sResponse= (String) clResponseDTO.getResponse();
      //  clResponseDTO.setResponse(parseResponse(sResponse));

        return clResponseDTO;
    }


  //  protected abstract Object parseResponse(String objResponse);

}
