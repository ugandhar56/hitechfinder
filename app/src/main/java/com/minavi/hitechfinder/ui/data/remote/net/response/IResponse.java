package com.minavi.hitechfinder.ui.data.remote.net.response;


import com.minavi.hitechfinder.ui.data.remote.net.dto.CLResponseDTO;

/**
 * Created by yugandhar on 10-11-2016.
 */
public interface IResponse<T>
{
    public static final byte TYPE_STRING=1;
    public static final byte TYPE_JSON=2;
    public static final byte TYPE_JSON_ARRAY=3;

    public static final byte STATUS_SUCCESS=1;
    public static final byte STATUS_ERROR=-1;

    public interface Listener<T>
    {
        public CLResponseDTO onResponse(byte byStatus, T Response);
    }
}
