package com.minavi.hitechfinder.ui.view.model;

/**
 * Created by yugandhar on 6/13/2017.
 */

public class RegisterDTO {

    String email;
    String first_name;
    String password;

    String mobile;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return first_name;
    }

    public void setUserName(String userName) {
        this.first_name = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRePassword() {
        return mobile;
    }

    public void setRePassword(String rePassword) {
        this.mobile = rePassword;
    }
}

