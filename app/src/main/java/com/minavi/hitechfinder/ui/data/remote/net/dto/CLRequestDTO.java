package com.minavi.hitechfinder.ui.data.remote.net.dto;



import com.google.gson.JsonObject;
import com.minavi.hitechfinder.ui.data.remote.net.request.IRequest;
import com.minavi.hitechfinder.ui.data.remote.net.response.IResponse;

import java.util.Map;

/**
 * Created by yugandhar on 10-11-2016.
 */
public class CLRequestDTO
{
    //Request URL
    private String sBaseURL;

    private String sURL;
    private JsonObject jsonObject;

    public JsonObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    //Request Parameters
    private Map<String, String> clParams=null;

//    private Map<String,Object> clobjParams=null;
    //Submit content type form/JSON/multipart, default: form
    private byte byContentType= IRequest.CONTENT_FORM; //form/JSON/multipart

    //Expected response from server
    private byte byResponseType= IResponse.TYPE_STRING;//TYPE: String,JSON,JSON_ARRAY

    //Request Method. default: post
    private byte byRequestMethod=IRequest.METHOD_POST;//Get/Post

    //JSON string in case byContentType=JSON
    private String sJsonString=null;

    private  Object[] objRequestCache=null;


    public String getBaseURL()
    {
        return sBaseURL;
    }

    public void setBaseURL(String sBaseURL) {
        this.sBaseURL = sBaseURL;
    }

    public String getURL() {
        return sBaseURL+sURL;
    }

    public void setURL(String sURL) {
        this.sURL = sURL;
    }


    public Map<String, String> getParams() {
        return clParams;
    }

    public void setParams(Map<String, String> clParams) {
        this.clParams = clParams;
    }

   /* public Map<String, Object> getClobjParams() {
        return clobjParams;
    }

    public void setClobjParams(Map<String, Object> clobjParams) {
        this.clobjParams = clobjParams;
    }*/

    public byte getContentType() {
        return byContentType;
    }

    public void setContentType(byte byContentType) {
        this.byContentType = byContentType;
    }

    public byte getResponseType() {
        return byResponseType;
    }

    public void setResponseType(byte byResponseType) {
        this.byResponseType = byResponseType;
    }

    public byte getRequestMethod() {
        return byRequestMethod;
    }

    public void setRequestMethod(byte byRequestMethod) {
        this.byRequestMethod = byRequestMethod;
    }

    public Object[] getRequestCache() {
        return objRequestCache;
    }

    public void setRequestCache(Object[] objRequestCache) {
        this.objRequestCache = objRequestCache;
    }

    public String getJsonString() {
        return sJsonString;
    }

    public void setJsonString(String sJsonString) {
        this.sJsonString = sJsonString;
    }
}
