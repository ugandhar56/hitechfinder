package com.minavi.hitechfinder.ui.view.callbacks;

import com.minavi.hitechfinder.ui.view.model.ServiceResponseDTO;

/**
 * Created by yugandhar on 7/2/2017.
 */

public interface InstutionsView
{
    public void handleInstutesResponce(ServiceResponseDTO serviceResponseDTO);
}
