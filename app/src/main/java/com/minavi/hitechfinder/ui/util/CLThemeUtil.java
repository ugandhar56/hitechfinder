package com.minavi.hitechfinder.ui.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.View;

import com.minavi.hitechfinder.ui.R;
import com.minavi.hitechfinder.ui.view.MainActivity;


/**
 * Created by J.yugandhar on 16-12-2016.
 */
public class CLThemeUtil {
    public static int sTheme;


    public static void changeToTheme(Activity activity, int theme)
    {
        sTheme = theme;
        activity.finish();
        activity.startActivity(new Intent((Context)activity, MainActivity.class));
    }

    /** Set the theme of the activity, according to the configuration. */
    public static void onActivityCreateSetTheme(Activity activity)
    {
       /* sTheme= CLSharedPreferencesHelper.getThemeId(activity);
        switch (sTheme)
        {
            default:
            activity.setTheme(R.style.CyanTheme);
            break;
            case IConstants.ITheme.CYAN:
                activity.setTheme(R.style.CyanTheme);
                break;
            case IConstants.ITheme.GREEN:
                activity.setTheme(R.style.GreenTheme);
                break;
            case IConstants.ITheme.TEAL:
                activity.setTheme(R.style.TealTheme);
                break;
            case IConstants.ITheme.PURPLE:
                activity.setTheme(R.style.PurpleTheme);
                break;
            case IConstants.ITheme.ORANGE:
                activity.setTheme(R.style.OrangeTheame);
                break;
            case IConstants.ITheme.LITE_BLUE:
                activity.setTheme(R.style.LightBlueTheme);
                break;
            case IConstants.ITheme.RED:
                activity.setTheme(R.style.RedTheme);
                break;
            case IConstants.ITheme.BROWN:
                activity.setTheme(R.style.BrownTheme);
                break;
            case IConstants.ITheme.LIME:
                activity.setTheme(R.style.LimeTheme);
                break;*/
            /*case IConstants.ITheme.YELLOW:
                activity.setTheme(R.style.yellow_theme);
                break;
*/
//        }
    }
    public static int getThemePrimaryColor (final Context context) {
        final TypedValue value = new TypedValue ();
        context.getTheme ().resolveAttribute (R.attr.colorPrimary, value, true);
        return value.data;
    }
    public static int getTitleTextColor (final Context context) {
        final TypedValue value = new TypedValue ();
        context.getTheme().resolveAttribute(R.attr.titleTextColor,value,true);
        return value.data;
    }
    public static int getThemeAssetColor (final Context context) {
        final TypedValue value = new TypedValue ();
        context.getTheme ().resolveAttribute (R.attr.colorAccent, value, true);
        return value.data;
    }
}
