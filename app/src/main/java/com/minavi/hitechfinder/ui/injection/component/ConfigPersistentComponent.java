package com.minavi.hitechfinder.ui.injection.component;


import com.minavi.hitechfinder.ui.injection.ConfigPersistent;
import com.minavi.hitechfinder.ui.injection.module.ActivityModule;
import com.minavi.hitechfinder.ui.injection.module.FragmentModule;

import dagger.Component;

/**
 * A dagger component that will live during the lifecycle of an Activity but it won't
 * be destroy during configuration changes. Check {@link BaseActivity} to see how this components
 * survives configuration changes.
 * Use the {@link ConfigPersistent} scope to annotate dependencies that need to survive
 * configuration changes (for example Presenters).
 */


@Component(dependencies = ApplicationComponent.class)
@ConfigPersistent
public interface ConfigPersistentComponent {

    FragmentComponent fragmentComponent(FragmentModule activityModule);
    ActivityComponent activityComponent(ActivityModule activityModule);

}