package com.minavi.hitechfinder.ui.base;

import org.json.JSONException;
import rx.Subscriber;

/**
 * Created by j.yugandhar on 06-10-2017.
 */
public interface IBaseObservable<T>
{
    public void callSubscriber(Subscriber<? super T> subscriber) throws JSONException;
}
