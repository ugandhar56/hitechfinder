package com.minavi.hitechfinder.ui.util;

import android.content.Context;
import android.os.Environment;

import java.io.*;

/**
 * Created by srikanth on 21/11/2016.
 */
public class FileUtil
{

    private final static String APP_RESOURCE_FOLDER="CentraCrm";

    private final  static String RES_FOLDER_IMAGES_NAME="images";
    private final  static String RES_FOLDER_LOGS_NAME="logs";
    private final  static String RES_FOLDER_ATTACHMENTS_NAME="attachments";

    public  final  static byte RES_FOLDER_IMAGES=1;
    public  final  static byte RES_FOLDER_LOGS=2;
    public  final  static byte RES_FOLDER_ATTACHMENTS=3;



    static
    {
        try
        {

            boolean success = true;

            /*Application clApplication=CLCrmApplication.getComponent().application();
            clApplication.getDir(APP_RESOURCE_FOLDER,Context.MODE_PRIVATE);
            */

            File file = null;
            file = new File(Environment.getExternalStorageDirectory() + File.separator+APP_RESOURCE_FOLDER);

            if (!file.exists())
                success = file.mkdir();


        }
        catch( Exception e  )
            {
                LogUtil.e(e,"Failed to create app resource folder "+APP_RESOURCE_FOLDER);
                /// NEED to do cshow msg dlg and exit app
                e.printStackTrace();
            }

    }

    public static File getResourcePath(byte byResType)
    {

        File clRootFile=null;

        /*Application clApplication=CLCrmApplication.getComponent().application();
        clRootFile=clApplication.getDir(APP_RESOURCE_FOLDER, Context.MODE_PRIVATE);
*/
        clRootFile = new File(Environment.getExternalStorageDirectory() + File.separator+APP_RESOURCE_FOLDER);
        File clFile=null;
        if(byResType==RES_FOLDER_IMAGES)
            clFile = new File(clRootFile,RES_FOLDER_IMAGES_NAME);
        else if(byResType==RES_FOLDER_LOGS)
            clFile = new File(clRootFile,RES_FOLDER_LOGS_NAME);
        else if(byResType==RES_FOLDER_ATTACHMENTS)
            clFile = new File(clRootFile,RES_FOLDER_ATTACHMENTS_NAME);

        return clFile;
    }


    /*
* Reads bytes from a file with given name
*/
    public static byte[] readFile(Context context, byte byResType, String sFileName) throws IOException
    {

            File clFile=getResourcePath(byResType);
            File clResourceFile=new File(clFile,sFileName);



            File file = new File(clResourceFile,sFileName);
            if(file.exists())
            {
                InputStream is = null;
                try
                {
                is = new FileInputStream(file);

                // Get the size of the file
                long length = file.length();

                // You cannot create an array using a long type.
                // It needs to be an int type.
                // Before converting to an int type, check
                // to ensure that file is not larger than Integer.MAX_VALUE.
                if (length > Integer.MAX_VALUE)
                throw new IOException("Could not completely read file " + file.getName() + " as it is too long (" + length + " bytes, max supported " + Integer.MAX_VALUE + ")");

                // Create the byte array to hold the data
                byte[] bytes = new byte[(int)length];

                // Read in the bytes
                int offset = 0;
                int numRead = 0;
                while (offset < bytes.length && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0)
                offset += numRead;

                // Ensure all the bytes have been read in
                if(offset < bytes.length)
                throw new IOException("Could not completely read file " + file.getName());

                return bytes;
            }
            finally
            {
            try {
            is.close();// Close the input stream
            } catch (IOException e) {}
            }
            }

        return null;
    }


    public static void createFile(Context context,byte byResType,String sResourceName,byte byData[]) throws Exception
    {
        File clFile=getResourcePath(byResType);
        File clResourceFile=new File(clFile,sResourceName);


        FileOutputStream fos=null;
        BufferedOutputStream bos = null;
        try {

            if(clResourceFile.exists())
                clResourceFile.delete();
            fos = new FileOutputStream(clResourceFile);
            bos = new BufferedOutputStream(fos);
            bos.write(byData);
        }
        catch(Exception e){}
        finally {
            if(bos != null) {
                try  {
                    //flush and close the BufferedOutputStream
                    bos.flush();
                    bos.close();
                } catch(Exception e){}
            }
        }






    }

    /*
    * Deletes given file or directory
    * */
    public static void deleteFile(Context context,byte byResType,String sResourceName) throws Exception
    {

        File clFile=getResourcePath(byResType);
        File clResourceFile=new File(clFile,sResourceName);


        if (clResourceFile.isDirectory()) {
            for (File child : clResourceFile.listFiles())
                deleteFile(context,byResType,sResourceName);
        }
        clResourceFile.delete();




    }


    public static String readAssetFile(Context context, String sFileName)
    {
        StringBuilder sBuilder = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(context.getAssets().open(sFileName), "UTF-8"));
            // do reading, usually loop until end of file reading
            String mLine = reader.readLine();
            while (mLine != null) {
                sBuilder.append(mLine);
                mLine = reader.readLine();
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return sBuilder.toString();
    }
}
