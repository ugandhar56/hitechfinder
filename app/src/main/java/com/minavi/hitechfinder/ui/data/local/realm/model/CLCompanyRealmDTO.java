package com.minavi.hitechfinder.ui.data.local.realm.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by yugandhar on 15-02-2017.
 */
public class CLCompanyRealmDTO extends RealmObject
{
    @PrimaryKey
    byte byRealmPk=1;

    int iCompanyId;
    String sCompanyCode;
    String sCompanyName;


    public String getCompanyName() {
        return sCompanyName;
    }

    public void setCompanyName(String sCompanyName) {
        this.sCompanyName = sCompanyName;
    }

    public String getCompanyCode() {
        return sCompanyCode;
    }

    public void setCompanyCode(String sCompanyCode) {
        this.sCompanyCode = sCompanyCode;
    }


    public int getCompanyId() {
        return iCompanyId;
    }

    public void setCompanyId(int iCompanyId) {
        this.iCompanyId = iCompanyId;
    }


}
