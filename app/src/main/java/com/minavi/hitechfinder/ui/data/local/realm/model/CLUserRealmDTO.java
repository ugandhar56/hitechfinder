package com.minavi.hitechfinder.ui.data.local.realm.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by yugandhar on 15-02-2017.
 */
public class CLUserRealmDTO extends RealmObject
{
    @PrimaryKey
    byte byRealmPk=1;

    private int iLoginId;
    private String sUserName;
    private String sDisplayName;
    private String sImageName;
    private String sDesignation;
    private  String sSessionId;

    public String getSessionId() {
        return sSessionId;
    }

    public void setSessionId(String sSessionId) {
        this.sSessionId = sSessionId;
    }

    public int getLoginId() {
        return iLoginId;
    }

    public void setLoginId(int iLoginId) {
        this.iLoginId = iLoginId;
    }

    public String getUserName() {
        return sUserName;
    }

    public void setUserName(String sUserName) {
        this.sUserName = sUserName;
    }

    public String getDisplayName() {
        return sDisplayName;
    }

    public void setDisplayName(String sDisplayName) {
        this.sDisplayName = sDisplayName;
    }

    public String getImageName() {
        return sImageName;
    }

    public void setImageName(String sImageName) {
        this.sImageName = sImageName;
    }

    public String getDesignation() {
        return sDesignation;
    }

    public void setDesignation(String sDesignation) {
        this.sDesignation = sDesignation;
    }

}
