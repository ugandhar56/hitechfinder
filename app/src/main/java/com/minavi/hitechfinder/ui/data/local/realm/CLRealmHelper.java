package com.minavi.hitechfinder.ui.data.local.realm;
;
import com.minavi.hitechfinder.ui.view.model.UserDTO;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by J.yugandhar on 29-12-2016.
 */
public class CLRealmHelper
{
    protected Realm realm;
    private static RealmConfiguration clRealmConfig=null;
    public static CLRealmHelper realmHealper=null;

 /*   static
    {
        clRealmConfig = new RealmConfiguration.Builder()
                .name("minavi.htf.realm")
                .schemaVersion(2)
                .deleteRealmIfMigrationNeeded()
                .build();
    }*/


    private CLRealmHelper()
    {
        //            Realm.init(CLCrmApplication.getComponent().context());
        RealmConfiguration myConfig = new RealmConfiguration.Builder()
                .name("minavi.hitech.realm")
                .schemaVersion(2)
                .deleteRealmIfMigrationNeeded()
                .build();


        realm=Realm.getInstance(myConfig);
    }


    public static CLRealmHelper getInstance()
    {
//        if(clRealmHelper ==null)
        {
            realmHealper =new CLRealmHelper();
        }

        return realmHealper;
    }


    public void SaveUsetDetails(UserDTO userDTO)
    {

        realm.beginTransaction();
//            realm.copyToRealm(clCompanyDTO);

        realm.copyToRealmOrUpdate(userDTO);
        realm.commitTransaction();

    }
    public UserDTO getUserDetails()
    {
        UserDTO clUserDTO=null;
        clUserDTO=realm.where(UserDTO.class).findFirst();
        return clUserDTO;

    }
    public void removeUser()
    {
        final RealmResults<UserDTO> clUser= realm.where(UserDTO.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm)
            {
                clUser.deleteAllFromRealm();
            }
        });
    }

}
