package com.minavi.hitechfinder.ui.data.remote.net;


import com.minavi.hitechfinder.ui.data.remote.net.dto.CLRequestDTO;
import com.minavi.hitechfinder.ui.data.remote.net.dto.CLResponseDTO;
import com.minavi.hitechfinder.ui.data.remote.net.request.CLJsonArrayRequest;
import com.minavi.hitechfinder.ui.data.remote.net.request.CLJsonObjectRequest;
import com.minavi.hitechfinder.ui.data.remote.net.request.CLStringRequest;
import com.minavi.hitechfinder.ui.data.remote.net.request.IRequest;
import com.minavi.hitechfinder.ui.data.remote.net.response.IResponse;

/**
 * Created by yugandhar on 11-11-2016.
 */
public class ConnectionManager {

    public static CLResponseDTO sendRequest(CLRequestDTO clRequestDTO)
    {
        IRequest clRequest=getRequestObject(clRequestDTO.getResponseType());
        return clRequest.sendRequest(clRequestDTO);
    }


    /*public static void enqueueRequest(CLRequestDTO clRequestDTO,IResponse.Listener clListener)
    {
        IRequest clRequest=getRequestObject(clRequestDTO.getContentType());
        clRequest.sendRequest(clRequestDTO, clListener);
    }*/


    private static IRequest getRequestObject(byte byRequestType)
    {
        IRequest clRequest=null;
        switch (byRequestType)
        {
            case IResponse.TYPE_JSON:
            {
                clRequest=new CLJsonObjectRequest();
                break;
            }
            case IResponse.TYPE_JSON_ARRAY:
            {
                clRequest=new CLJsonArrayRequest();
                break;
            }
            default:
            {
                clRequest=new CLStringRequest();
                break;
            }
        }

        return clRequest;
    }
}
