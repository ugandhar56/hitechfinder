package com.minavi.hitechfinder.ui.base;

/**
 * Base class that implements the IPresenter interface and provides a base implementation for
 * attachView() and detachView(). It also handles keeping a reference to the mvpView that
 * can be accessed from the children classes by calling getMvpView().
 */
public class BasePresenter<T extends IView> implements IPresenter<T> {

    private T clView;

    @Override
    public void attachView(T clView) {
        this.clView = clView;
    }

    @Override
    public void detachView() {
        clView = null;
    }

    public boolean isViewAttached() {
        return clView != null;
    }

    public T getMvpView() {
        return clView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new CLViewNotAttachedException ();
    }

    public static class CLViewNotAttachedException extends RuntimeException
    {
        public CLViewNotAttachedException ()
        {
            super("Please call IPresenter.attachView(IView) before" +
                    " requesting data to the IPresenter");
        }
    }
}

