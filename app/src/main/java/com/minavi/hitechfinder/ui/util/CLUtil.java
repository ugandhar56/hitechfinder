package com.minavi.hitechfinder.ui.util;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by srikanth on 21/11/2016.
 */
public class CLUtil
{

    /*
* Returns whether given string numeric or not
*/
    public static boolean isNumeric(String str)
    {
        //return str.matches("-?\\d+(.\\d+)?");
        return str.matches("[-+]?\\d+(\\.\\d+)?");
    }

    /*
	* Packs given bit in integer
	*/
    public static int PackBit(int value, byte bitNo)
    {
        value = value | (1 << bitNo);
        return value;
    }

    /*
    * Returns whether given bit is packed in given value or not
    */
    public static boolean CheckBit(int value, byte bitNo)
    {
        int ivalue = value;
        return (ivalue & (1 << bitNo)) > 0;
    }








}
