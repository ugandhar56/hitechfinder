package com.minavi.hitechfinder.ui.util;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minavi.hitechfinder.ui.drawables.FontIconDrawable;

/**
 * Three different ways this FontIconUtil can be used
 * 1= GetTypeFace and set the same to testView and set the icon res id as setText
 * 2= Mark all the TextViews in a container with FontIcon using the value set as setText
 * 3= Drawable can be fetched and use the same with setBackgroundDrawable
 * Created by J.yugandhar on 06-07-2016.
 */
public class FontIconUtil
{
    public static final String ROOT = "fonts/";
           // FONTAWESOME = ROOT + "fontawesome-webfont.ttf",FONTMOON=ROOT+"icomoon.ttf";

    public static  final String DEFAULT_FONT=ROOT+"fontawesome.ttf";

    /**
     * To mark all the TextViews in the container with Font Icon TypeFace
     * @param v
     * @param typeface
     */
    public static void markAsIconContainer(View v, Typeface typeface)
    {
        //View childd;
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                View  child= vg.getChildAt(i);
                      markAsIconContainer(child,typeface);
            }
        } else if (v instanceof TextView) {
            ((TextView) v).setTypeface(typeface);
        }
    }

    public static Typeface getTypeface(Context context, String font) {

        return Typeface.createFromAsset(context.getAssets(), font);
    }

    public static Drawable getFontDrawable(Context context,int size,String sResIconId,int iColor)
    {
        FontIconDrawable faIcon = new FontIconDrawable(context);
        faIcon.setTextSize(TypedValue.COMPLEX_UNIT_DIP, size);
        faIcon.setTextAlign(Layout.Alignment.ALIGN_OPPOSITE);
        faIcon.setTextColor(iColor);
        faIcon.setTypeface(FontIconUtil.getTypeface(context, FontIconUtil.DEFAULT_FONT));
        faIcon.setText(sResIconId);
        return faIcon;
    }
    public static Drawable getFontDrawable(Context context,int size,String sResIconId)
    {
       return getFontDrawable(context,size,sResIconId,CLThemeUtil.getThemeAssetColor(context));
    }


}
