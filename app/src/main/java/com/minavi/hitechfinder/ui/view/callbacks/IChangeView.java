package com.minavi.hitechfinder.ui.view.callbacks;

import com.minavi.hitechfinder.ui.view.model.ServiceResponseDTO;

/**
 * Created by yugandhar on 7/2/2017.
 */

public interface IChangeView {
   public void handleChangePassword(ServiceResponseDTO serviceResponseDTO);
}
