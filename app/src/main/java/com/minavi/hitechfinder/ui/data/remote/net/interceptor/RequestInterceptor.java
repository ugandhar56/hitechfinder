package com.minavi.hitechfinder.ui.data.remote.net.interceptor;

import android.util.Log;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by yugandhar on 11-11-2016.
 */
public class RequestInterceptor implements Interceptor{

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        long t1 = System.nanoTime();
        Log.i("UrlReq","sending request:"+request.url());//request.url(), chain.connection(), request.headers()));

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        Log.i("UrlReq","response time: "+((t2 - t1) / 1e6d)+"(url:"+request.url()+")");

        /*logger.info(String.format("Received response for %s in %.1fms%n%s",
                response.request().url(), (t2 - t1) / 1e6d, response.headers()));*/

        return response;
    }
}
