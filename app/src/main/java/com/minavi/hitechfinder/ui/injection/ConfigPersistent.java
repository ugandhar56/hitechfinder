package com.minavi.hitechfinder.ui.injection;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;



/**
 * A scoping annotation to permit dependencies conform to the life of the
 * {@link CLConfigPersistentComponent}
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigPersistent {
}
