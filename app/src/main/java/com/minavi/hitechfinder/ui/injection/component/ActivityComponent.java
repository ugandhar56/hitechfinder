package com.minavi.hitechfinder.ui.injection.component;


import com.minavi.hitechfinder.ui.injection.PerActivity;
import com.minavi.hitechfinder.ui.injection.module.ActivityModule;

import dagger.Subcomponent;

//import com.focus.centra.crm.ui.sample.CLSecondActivity;


/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

  /*  void inject(CLTabHomeActivity deckActivity);

    void inject(CLSplashScreenActivity clSplashScreenActivity);
    void inject(CLConfigurationActivity clConfigurationActivity);
    void inject(CLLoginActivity clLogInActivity);
    void inject(CLMobileHomeActivity clMobileHomeActivity);
*///    void inject(CLMainActivity clSecondActivity);
//    void inject(CLDetailedViewFragment clDetailedViewFragment);
//    void inject(CLSecondActivity clSecondActivity);



}
