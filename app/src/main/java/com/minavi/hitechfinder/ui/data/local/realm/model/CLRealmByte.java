package com.minavi.hitechfinder.ui.data.local.realm.model;

import io.realm.RealmObject;

/**
 * Created by J.yugandhar on 23-01-2017.
 */
public class CLRealmByte extends RealmObject {
    private byte val;

    public CLRealmByte() {
    }

    public CLRealmByte(byte val) {
        this.val = val;
    }
}
