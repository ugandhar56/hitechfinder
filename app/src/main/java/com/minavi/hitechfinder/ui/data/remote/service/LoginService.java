package com.minavi.hitechfinder.ui.data.remote.service;

import com.google.gson.Gson;
import com.minavi.hitechfinder.ui.base.BaseObservable;
import com.minavi.hitechfinder.ui.view.model.LoginDTO;
import com.minavi.hitechfinder.ui.view.model.ServiceResponseDTO;
import com.minavi.hitechfinder.ui.data.remote.net.ConnectionManager;
import com.minavi.hitechfinder.ui.data.remote.net.dto.CLRequestDTO;
import com.minavi.hitechfinder.ui.data.remote.net.dto.CLResponseDTO;
import com.minavi.hitechfinder.ui.data.remote.net.request.IRequest;

import rx.Subscriber;
import rx.Observable;

/**
 * Created by yugandhar on 6/13/2017.
 */

public class LoginService extends CLBaseService implements ILoginService {

    @Override
    public Observable authenticateUser(final LoginDTO loginDTO) {
        Observable clObservable = Observable.create(new BaseObservable() {
            @Override
            public void callSubscriber(Subscriber subscriber) {
                CLRequestDTO clRequestDTO = getNewRequestDTO();
                clRequestDTO.setRequestMethod(IRequest.METHOD_POST);
                Gson gson = new Gson();
                clRequestDTO.setJsonString(gson.toJson(loginDTO));
                //clRequestDTO.setJsonString(clRequestDTO.getJsonString().substring(1,clRequestDTO.getJsonString().length()-1));
                clRequestDTO.setContentType(IRequest.CONTENT_JSON);
                clRequestDTO.setURL("/auth/login");
                CLResponseDTO clResponseDTO= ConnectionManager.sendRequest(clRequestDTO);

                subscriber.onNext(gson.fromJson((String) ConnectionManager.sendRequest(clRequestDTO).getResponse(), ServiceResponseDTO.class));
                subscriber.onCompleted();
            }

        });
        return clObservable;

    }
}

    /*private JsonObject getJsonObject()

    {
        JsonObject jsonObject=null;
        try {
            jsonObject=new JsonObject();
            jsonObject.put("email", mEmail.getText().toString());
            jsonObject.put("password", mPassword.getText().toString());
            jsonObject.put("gender", gender);
            Log.d(TAG, "registerApiCall: " + jsonObject.toString());        }catch (JsonIOException je)
        {
            je.printStackTrace();
        }

        }


}*/
