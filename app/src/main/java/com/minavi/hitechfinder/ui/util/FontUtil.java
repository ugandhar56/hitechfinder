package com.minavi.hitechfinder.ui.util;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;

import java.lang.reflect.Field;


/**
 * Created by R.Saiteja on 27-01-2017.
 */
public class FontUtil {


   /* public static void setDefaultFont(Context context, String staticTypefaceFieldName, String fontAssetName) {
        final Typeface regular = Typeface.createFromAsset(context.getAssets(), fontAssetName);
        replaceFont(staticTypefaceFieldName, regular);
    }*/

    public static void replaceFont(String staticTypefaceFieldName, final Typeface newTypeface) {
        try {
            final Field staticField = Typeface.class.getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void setDefaultFont(Context clContext) {
        String sTypefaceFieldName = "MONOSPACE";
        String sChangeFont_key="Font Setting";
        String sSansSerif;
        int iTypeface;

        String sFontString = PreferenceManager.getDefaultSharedPreferences(clContext).getString(sChangeFont_key, null);
        if (sFontString == null) {
            Typeface clTypeface=Typeface.createFromAsset(clContext.getAssets(), FontIconUtil.DEFAULT_FONT);//"fonts/fontawesome.ttf"
            replaceFont(sTypefaceFieldName, clTypeface);
        }
        else {
            String sSplitedValue[]=sFontString.split(",");
            sSansSerif=sSplitedValue[0].trim();
            iTypeface= Integer.parseInt(sSplitedValue[1].trim());
            FontUtil.replaceFont(sTypefaceFieldName, Typeface.create(sSansSerif,iTypeface));

        }

    }



    public  static void saveTypeFaceValues(Context clContext, int iTypeface,String sSanceSerif){

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(clContext);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("ITYPEFACE", iTypeface);
            editor.putString("SANCESERIF", sSanceSerif);
            editor.commit();
    }

    public  static Object[] getTypeFaceValues(Context clContext){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(clContext);
        String sSanceSerif=preferences.getString("SANCESERIF", "sans-serif");
        int iTypeface=preferences.getInt("ITYPEFACE", 0);

        Object[] objValues = new Object[2];

        objValues[0] = sSanceSerif;
        objValues[1] = iTypeface;

        return objValues;


    }
/*
public  static void saveTypeFaceField(Context clContext, int clTypeface){

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(clContext);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("TYPEFACE", clTypeface);
            editor.commit();
    }

    public  static Typeface getTypeFaceField(Context clContext){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(clContext);
        int sTypeface=preferences.getInt("TYPEFACE", 0);
        return Typeface.defaultFromStyle(sTypeface);


    }
*/


}
