package com.minavi.hitechfinder.ui.data.local.sqlite;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by n.satish on 25-11-2016,12:18.
 */
public class CLBaseSQLiteOpenHelper extends SQLiteOpenHelper
{

    private SQLiteDatabase mDatabase;
    private AtomicInteger mOpenCounter = new AtomicInteger();

    private static final String DB_NAME = "Assem.db";
    private static final String Tab_mCore_Account = "mCore_Account";
    private static final String Tab_muCore_Account = "muCore_Account";
    private static final String Tab_mCore_Source = "mCore_Source";
    private static final String sClientColName= "iClientId";

    private static final int DB_VERSION = 1;

    //private static CLDBStore clDBStore = null;

    public CLBaseSQLiteOpenHelper(Context context)
    {
        super(context, DB_NAME , null, DB_VERSION); // super(Context context, String name,SQLiteDatabase.CursorFactory factory, int version)
    }

    public synchronized SQLiteDatabase getReadableDatabase()
    {
        return getDatabase(false);
    }

    public synchronized SQLiteDatabase getWritableDatabase()
    {
        return getDatabase(true);
    }

    /**
     * Return the readable/writable SQLiteDatabase object
     */
    public synchronized  SQLiteDatabase getDatabase(boolean isForWrite)
    {
        try
        {
            if(mOpenCounter.incrementAndGet() == 1)
            {
                if(isForWrite)
                    mDatabase = super.getWritableDatabase();
                else mDatabase = super.getReadableDatabase();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mDatabase;
    }

    public synchronized void closeDatabase()
    {
        if(mOpenCounter.decrementAndGet() == 0 || mOpenCounter.get()<0)
        {
            // Closing database
            if(mDatabase != null )
            {
                mOpenCounter.set(0);
                mDatabase.close();
                mDatabase = null;
            }
        }
    }

    public CLBaseSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public CLBaseSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
