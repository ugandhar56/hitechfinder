package com.minavi.hitechfinder.ui.base;

/**
 * Every presenter in the app must either implement this interface or extend BasePresenter
 * indicating the IView type that wants to be attached with.
 */
public interface IPresenter<T extends IView> {

    void attachView(T mvpView);

    void detachView();
}
