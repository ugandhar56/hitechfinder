package com.minavi.hitechfinder.ui.comp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.minavi.hitechfinder.ui.R;
import com.minavi.hitechfinder.ui.base.IView;


/**
 * Created by srikanth on 14/11/2016.
 */
public class ProgressHandler
{
        public final static byte SHOW_ON_SCREEN=1;
        public final static byte SHOW_ON_TOOLBAR=2;
/*        public final byte SHOW_VIEWGROUP=3;*/

        private static ProgressHandler progressHandler =null;

        private ProgressBar mProgressBar;
        private Context mContext;

       private ProgressHandler()
       {

       }

       public static ProgressHandler getInstance()
       {
             if(progressHandler ==null)
                 progressHandler =new ProgressHandler();

           return progressHandler;

       }


       public void show(IView clView, byte byShowType )
       {

           Context clContext=null;

           if(clView instanceof Fragment)
               clContext=((Fragment)clView).getActivity();
           else
               clContext=(Activity)clView;


           if(byShowType==SHOW_ON_SCREEN)
            {
                ViewGroup clRecentViewGroup=null;
                if(mProgressBar==null)
                {



                    //mProgressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal);
                    //mProgressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleLarge);
                    mProgressBar = new ProgressBar(clContext, null, android.R.attr.progressBarStyleLarge);
                    mProgressBar.setIndeterminate(true);
                    //   mProgressBar.setProgressStyle(android.R.attr.progressBarStyleHorizontal);
                    //mProgressBar.setProgressDrawable(context.getDrawable(R.android.circular_progressbar));


                }
               else
                {
                    clRecentViewGroup=((ViewGroup) mProgressBar.getParent());
                    clRecentViewGroup.removeView(mProgressBar);
                }

                ViewGroup   clViewGroup = (ViewGroup) ((Activity) clContext).findViewById(android.R.id.content).getRootView();
                if(clRecentViewGroup==null || !clRecentViewGroup.equals(clViewGroup))
                {
                    attachToViewGroup(mProgressBar, clContext, clViewGroup);
                }


//                ((Activity) clContext).getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                mProgressBar.setVisibility(View.VISIBLE);


             }
            else
            {

            }


       }


      public void  attachToViewGroup(Context clContext,ViewGroup clViewGroup)
      {
          ProgressBar clProgressBar = new ProgressBar(clContext, null, android.R.attr.progressBarStyleLargeInverse);
          clProgressBar.setIndeterminate(true);

          attachToViewGroup(clProgressBar, clContext, clViewGroup);
          clProgressBar.setVisibility(View.VISIBLE);

      }

    public void detachFromViewGroup(Context clContext, ViewGroup clViewGroup)
    {
        RelativeLayout clRelativeLayout=(RelativeLayout)clViewGroup.findViewById(R.id.progress_layout);
        ProgressBar clProgressBar=(ProgressBar)clRelativeLayout.getChildAt(0);
        clProgressBar.setVisibility(View.INVISIBLE);
        clRelativeLayout.removeView(clProgressBar);
        clViewGroup.removeView(clRelativeLayout);
        clViewGroup.setEnabled(true);
        clRelativeLayout=null;
        clProgressBar=null;


    }

    private void attachToViewGroup(ProgressBar clProgressBar,Context clContext,ViewGroup clViewGroup)
    {


        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        RelativeLayout rl = new RelativeLayout(clContext);
        rl.setId(R.id.progress_layout);
        rl.setGravity(Gravity.CENTER);
        rl.addView(clProgressBar);
        clViewGroup.setBackgroundColor(Color.WHITE);
        clViewGroup.addView(rl, params);
        clViewGroup.setEnabled(false);


    }




      /*  public void show() {
            mProgressBar.setVisibility(View.VISIBLE);
        }*/

        public void hide() {

           if(mProgressBar!=null)
           {
               ((Activity) mProgressBar.getContext()).getWindow().clearFlags((WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE));
               mProgressBar.setVisibility(View.INVISIBLE);
           }
        }






}

/*
ProgressBar pb = new ProgressBar(context,
                                 null,
                                 android.R.attr.progressBarStyleHorizontal);
 */

/*
mProgressBarHandler = new ProgressBarHandler(this); // In onCreate
mProgressBarHandler.show(); // To show the progress bar
mProgressBarHandler.hide(); // To hide the progress bar
 */