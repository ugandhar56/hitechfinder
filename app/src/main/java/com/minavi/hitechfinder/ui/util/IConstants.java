package com.minavi.hitechfinder.ui.util;

/**
 * Created by J.yugandhar on 16-12-2016.
 */
public interface IConstants {
    public static short APP_ID = 244;

    interface IField {
        //Data Types

        interface IDataType {
            public static final byte TEXT = 0;
            public static final byte NUMBER = 1;
            public static final byte BOOLEAN = 2;

            public static final byte DATETIME = 3;

            public static final byte DATE = 4;
            public static final byte TIME = 5;

            public static final byte FRACTION = 6;

            public static final byte PICTURE = 7;

            public static final byte STRINGLIST = 8;
            public static final byte NUMBERLIST = 9;

            public static final byte DOCVIEWER = 10;
            public static final byte UPDATEDTIME = 11;
            public static final byte MASTER = 12;
            public static final byte BIGNUMBER = 13;
            public static final int USER=2564;
        }

        interface IOperator
        {
            public static final byte EQUALTO = 0;
            public static final byte NOTEQUALTO = 1;
            public static final byte LESSTHAN = 2;
            public static final byte GREATERTHAN = 3;
            public static final byte BETWEEN= 16;
            public static final byte LESSTHANOREQUALTO = 4;
            public static final byte GREATERTHANOREQUALTO = 5;

            public static final byte CONTAINING = 6;
            public static final byte NOTCONTAINING = 7;

            public static final byte ISBLANK = 8;
            public static final byte ISNOTBLANK = 9;
        }
    }


    public interface IMenuConstants
    {
        public static final byte Reports = 0;
        public static final byte Master = 10;
        public static final byte Transactions = 20;
        public static final byte Dashboard = 4;
        public static final byte Approvals = 2;
        public static final byte Alerts = 1;
        public static final byte Price_Book = 3;
        public static final byte Stock_Check = 5;
    }

    public interface ITheme
    {
        public static final byte TEAL = 0;
        public static final byte GREEN = 1;
        public static final byte CYAN = 2;
        public static final byte PURPLE = 3;
        public static final byte ORANGE = 4;
        public static final byte LITE_BLUE = 5;
        public static final byte RED = 6;
        public static final byte BROWN = 7;
        public static final byte LIME = 8;
        public static final byte YELLOW = 9;
    }


    public interface IDashboard
    {
        public final static byte DASHLET_GRAPH = 1;
        public final static byte DASHLET_REPORTS = 2;
        public final static byte DASHLET_RSSFEEDS = 3;
        public final static byte DASHLET_WEBLINK = 4;
        public final static byte DASHLET_ALERTS = 5;
        public final static byte DASHLET_MAP = 6;
        public final static byte DASHLET_HTML = 7;
        public final static byte DASHLET_METRIC = 8;
        public final static byte DASHLET_IMAGE = 9;
        public final static byte DASHLET_COMPOSITE_GRAPH = 10;
        public final static byte DASHLET_COMPOSITE_REPORT = 11;
        public final static byte DASHLET_ANNOUNCEMENT = 12;
        public final static byte DASHLET_MESSAGES = 13;
        public final static byte DASHLET_FOCUS_REPORTS = 14;
        public final static byte DASHLET_CALENDAR = 15;
        public final static byte DASHLET_GROUP_CAROSUEL = 16;
        public final static byte DASHLET_NOTIFICATIONS = 17;
        public final static byte DASHLET_SHORTCUTS = 18;
        public final static byte DASHLET_HASHTAGS = 19;
        public final static byte DASHLET_RECENTITEMS = 20;
        public final static byte DASHLET_GANTTCHART = 21;
    }

    public interface IChart
    {
        public final byte TYPE_BAR = 1;
        public final byte TYPE_BAR_HORIZONTAL = 2;
        public final byte TYPE_PIE = 3;
        public final byte TYPE_LINE = 4;
        public final byte TYPE_AREA = 5;
        public final byte TYPE_FUNNEL = 6;
        public final byte TYPE_BUBBLE = 7;
        public final byte TYPE_GUAGE = 8;
        public final byte TYPE_DONUT = 12;
        public final byte TYPE_BAR_STACKED = 13;

        public final byte TYPE_BOX = 9;
        public final byte TYPE_BOX_TOTAL = 10;

        public final byte TYPE_3D_STACKED = 11;
        public final byte TYPE_METRIC = 14;
        public final byte TYPE_GEO_CHART_COUNTRIES = 15;
        public final byte TYPE_GEO_CHART_CITIES = 16;

        interface ILegend {
            public final byte NONE = 0;
            public final byte TOP = 1;
            public final byte BOTTOM = 2;
            public final byte LEFT = 3;
            public final byte RIGHT = 4;
        }
    }

    public interface ILocalStorageType {
        public final byte SHARED_PREFERENCES = 1;
        public final byte SQLITE = 2;
        public final byte REALM = 3;
    }

    public interface IDisplayType
    {
        public static final byte TABULAR=0;
        public static final byte LIST=1;
    }
    public enum URL {
        LOGIN("/mservices/Login"),
        REQ_GET_LIST_VIEW("/mservices/ListView/getListView"),
        REQ_GET_DETAILED_VIEW_HEADER("/mservices/DetailedView/getDetailedViewHeader"),
        REQ_GET_RELATED_MODULES("/mservices/DetailedView/getRelatedModules"),
        REQ_GET_STREAM_ACTIVITY("/mservices/DetailedView/getActivityStream");

        URL(String sUrl) {
            this.sUrl = sUrl;
        }

        private String sUrl;

        public String getURL() {
            return sUrl;
        }

    }


}
