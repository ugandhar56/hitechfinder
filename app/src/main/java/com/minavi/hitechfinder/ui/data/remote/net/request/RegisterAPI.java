package com.minavi.hitechfinder.ui.data.remote.net.request;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit2.http.Headers;

/**
 * Created by yugandhar on 6/17/2017.
 */

public interface RegisterAPI {

    @Headers({
            "Accept: application/json"
    })
    @FormUrlEncoded
//    @Headers("Content-Type: application/json")
    @POST("/auth/user")
    public void insertUser(
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("email") String email,
            @Field("password") String password,
            Callback<Response> callback);

    //need add these fields later
   /* @Field("password") String password,
    @Field("mobile") String mobile,
    @Field("gender") String gender,
    @Field("dob") String dob,*/

    //login requset
    @Headers({
            "Accept: application/json"
    })
    @FormUrlEncoded
//    @Headers("Content-Type: application/json")
    @POST("/auth/login")
    public void loginUser(
            @Field("email") String email,
            @Field("password") String password,
            Callback<Response> callback);

//resetPassword
@Headers({
        "Accept: application/json"
})
    @FormUrlEncoded
    @POST("/auth/change-password")
    public void resetPassword(
//            @Header("Content-Type") String contentType,
            @Field("email") String email,
            @Field("password") String password,
            Callback<Response> callback);
    @Headers({
            "Accept: application/json"
    })
    @FormUrlEncoded
    @POST("/auth/forgot_password")
//    @Headers("Content-Type: application/json")
    public void forgetPassword(
            @Field("email") String email,
            Callback<Response> callback);
}
