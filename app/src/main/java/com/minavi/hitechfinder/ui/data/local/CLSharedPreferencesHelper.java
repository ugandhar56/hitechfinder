package com.minavi.hitechfinder.ui.data.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by yugandhar on 20-01-2016.
 */
public class CLSharedPreferencesHelper
{
    public static void saveServerDetails(Context context, String sServerAddress, int iPortNo)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("SERVER_ADDR", sServerAddress);
        editor.putInt("PORT_NO", iPortNo);
        editor.commit();
    }

    public static Object[] getServerDetails(Context context)
    {
        Object[] objValues=null;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String sServer=preferences.getString("SERVER_ADDR", null);

        if(sServer!=null && sServer.trim().length()>0)
        {
            objValues=new Object[2];
            objValues[0] = sServer;
            objValues[1] = preferences.getInt("PORT_NO", 0);
        }
        return objValues;
    }

    public static String getServerAddress(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String sServer=preferences.getString("SERVER_ADDR", null);
        return sServer;
    }

    public static int getPortNo(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt("PORT_NO", 0);
    }

    public static void saveCompanyDetails(Context context, String sCompanyName, String sCompanyCode)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("COMP_NAME", sCompanyName);
        editor.putString("COMP_CODE", sCompanyCode);
        editor.commit();
    }

    public static void saveCompanyCode(Context context, String sCompanyCode)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("COMP_CODE", sCompanyCode);
        editor.commit();
    }

    public static String[] getCompanyDetails(Context context)
    {
        String[] sCompanyDetails=new String[2];
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        sCompanyDetails[0]=preferences.getString("COMP_NAME",null);
        sCompanyDetails[1]=preferences.getString("COMP_CODE",null);
        return sCompanyDetails;
    }

    public static void clearCompanyDetails(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("COMP_NAME");
        editor.remove("COMP_CODE");
        editor.commit();
    }

    public static String getCompanyName(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString("COMP_NAME",null);
    }

    public static String getCompanyCode(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString("COMP_CODE",null);
    }

    public static void saveLoginDetails(Context context, String sLoginName, String sPassword)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("LOGIN_NAME", sLoginName);
        editor.putString("LOGIN_PWD", sPassword);
        editor.commit();
    }

    public static String[] getLoginDetails(Context context)
    {
        String[] sLoginDetails=new String[2];
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        sLoginDetails[0]=preferences.getString("LOGIN_NAME",null);
        if(sLoginDetails[0]==null)
            return null;
        sLoginDetails[1]=preferences.getString("LOGIN_PWD",null);
        return sLoginDetails;
    }

    public static void clearLoginDetails(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("LOGIN_NAME");
        editor.remove("LOGIN_PWD");
        editor.commit();
    }


    public static boolean isServerConfigured(Context context)
    {
        return getServerDetails(context)!=null;
    }

    public static boolean isUserLoggedIn(Context context)
    {
        return getLoginDetails(context)!=null;
    }

    public static void setTheme(Context context,int iThemeId)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("THEME_ID", iThemeId);
        editor.commit();

    }

    public static  int getThemeId(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt("THEME_ID", 0);
    }
    public static void setParms(Context context,int width,int height,int ix,int iy)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("WIDTH", width);
        editor.putInt("HEIGHT",height);
        editor.putInt("IX",ix);
        editor.putInt("IY",iy);
        editor.commit();

    }
    public static  int[] getPrams(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        int parms[]=new int[4];
        parms[0]=preferences.getInt("WIDTH", 0);
        parms[1]=preferences.getInt("HEIGHT", 0);
        parms[2]=preferences.getInt("IX", 0);
        parms[3]=preferences.getInt("IY", 0);
        return parms;
    }
}
