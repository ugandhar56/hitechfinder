package com.minavi.hitechfinder.ui.view.model;

/**
 * Created by yugandhar on 6/13/2017.
 */

public class LoginDTO {

    String email;
    String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
