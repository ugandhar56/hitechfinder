package com.minavi.hitechfinder.ui.data.remote.net.request;

/**
 * Created by yugandhar on 6/17/2017.
 */

public interface RequestConstants
{

    public  static String ROOT_URL="http://beta.hitechfinder.com";
    public  static String SIGNUP_URL=ROOT_URL+"/auth/user";
    public  static String LOGIN_URL=ROOT_URL+"/auth/login";
    public  static String FORGOT_PASSWORD_URL=ROOT_URL+"/auth/forgot_password";
    public  static String LOGOUT=ROOT_URL+"/auth/logout";
    public  static String CHANGE_PASSWORD_URL=ROOT_URL+"/auth/change-password";
    public  static String INSTUTATES_URL=ROOT_URL+"/api/institutes";


    public static interface Responce
    {
        public static final int RESPONCE_SUCESS=200;
        public static final int RESPONCE_ERROR=500;

    }

}
