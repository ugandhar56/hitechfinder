package com.minavi.hitechfinder.ui.view.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class ServiceResponseDTO<T> implements Serializable
{

    @SerializedName("status")
    int iStatus;
    @SerializedName("msg")
    String sStatusMsg;
    @SerializedName("response")
    T objResponse;

    private  Object[] objRequestCache;


    public T  getResponse() {
        return objResponse;
    }

    public void setResponse(T  oResponse) {

        this.objResponse = oResponse;
    }

    public int getStatus() {
        return iStatus;
    }

    public void setStatus(int iStatus) {
        this.iStatus = iStatus;
    }

    public String getStatusMsg() {
        return sStatusMsg;
    }

    public void setStatusMsg(String sStatusMsg) {
        this.sStatusMsg = sStatusMsg;
    }


    public Object[] getRequestCache() {
        return objRequestCache;
    }

    public void setRequestCache(Object[] objRequestCache) {
        this.objRequestCache = objRequestCache;
    }


}
