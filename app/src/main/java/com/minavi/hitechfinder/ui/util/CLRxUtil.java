package com.minavi.hitechfinder.ui.util;

import rx.Subscription;

public class CLRxUtil {

    public static void unsubscribe(Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
