package com.minavi.hitechfinder.ui.view.callbacks;

import com.minavi.hitechfinder.ui.base.IView;
import com.minavi.hitechfinder.ui.view.model.ServiceResponseDTO;

/**
 * Created by yugandhar on 6/13/2017.
 */

public interface ILoginView extends IView{

    public void handleLoginResponce(ServiceResponseDTO serviceResponseDTO);
    public void handleForgetResponce(ServiceResponseDTO serviceResponseDTO);


}
