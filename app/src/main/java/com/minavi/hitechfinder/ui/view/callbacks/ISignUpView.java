package com.minavi.hitechfinder.ui.view.callbacks;

import com.minavi.hitechfinder.ui.view.model.ServiceResponseDTO;

/**
 * Created by yugandhar on 6/17/2017.
 */

public interface ISignUpView {

    public void handleSignUpResponce(ServiceResponseDTO serviceResponseDTO);

}
