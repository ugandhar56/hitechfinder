package com.minavi.hitechfinder.ui.data.remote.net.request;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.minavi.hitechfinder.ui.HTFAppilication;
import com.minavi.hitechfinder.ui.R;
import com.minavi.hitechfinder.ui.data.local.realm.CLRealmHelper;
import com.minavi.hitechfinder.ui.data.remote.service.ILoginService;
import com.minavi.hitechfinder.ui.view.LoginActivity;
import com.minavi.hitechfinder.ui.view.callbacks.IChangeView;
import com.minavi.hitechfinder.ui.view.callbacks.InstutionsView;
import com.minavi.hitechfinder.ui.view.model.InstitutesDTO;
import com.minavi.hitechfinder.ui.view.model.LoginDTO;
import com.minavi.hitechfinder.ui.view.model.RegisterDTO;
import com.minavi.hitechfinder.ui.view.model.ServiceResponseDTO;
import com.minavi.hitechfinder.ui.view.model.UserDTO;
import com.minavi.hitechfinder.ui.util.DilogUtil;
import com.minavi.hitechfinder.ui.view.callbacks.ILoginView;
import com.minavi.hitechfinder.ui.view.callbacks.ISignUpView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by yugandhar on 6/17/2017.
 */

public class HTFController {
    public Context context;

    //Creating object for our interface
    public HTFController(Context context) {
        this.context = context;
    }

    public void logInAPICall(final ILoginView loginView, LoginDTO loginDTO)
    {
        final DilogUtil util = new DilogUtil(context);
        if (isNetworkAvailable(context))
        {
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait");
            pDialog.show();
            Gson gson = new GsonBuilder().create();
            String json = gson.toJson(loginDTO);
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    RequestConstants.LOGIN_URL, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());
                            pDialog.dismiss();
                            //User json from json response
                            JSONObject jsonUser = response.optJSONObject("user");
                            try {
                                UserDTO userDTO= new Gson().fromJson(jsonUser.toString(), UserDTO.class);
                                CLRealmHelper.getInstance().SaveUsetDetails(userDTO);
                                ServiceResponseDTO serviceResponseDTO=new ServiceResponseDTO();
                                serviceResponseDTO.setStatusMsg(response.getString("message"));
                                loginView.handleLoginResponce(serviceResponseDTO);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    // hide the progress dialog
                    pDialog.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if (response != null && response.data != null) {
                        String json = new String(response.data);
                        try {
                            JSONObject object = new JSONObject(json);
                            String message = object.optString("error");
                            if (message == null || TextUtils.isEmpty(message)) {
                                message = object.optString("message");
                            }
                            if (message == null || TextUtils.isEmpty(message)) {
                                message = object.optString("status");
                            }
                            util.showDialog(message, context);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        util.showDialog(context.getResources().getString(R.string.notAbleToLoginTrylater),context);
                    }
                }
            });
            //Register API calling
            HTFAppilication.get(context).addToRequestQueue(jsonObjReq);
        }
        else
        {
            util.showDialog(context.getResources().getString(R.string.pleaseCheckInternet), context);
        }
    }
    public void SignUPAPICall(final ISignUpView signUpView, RegisterDTO registerDTO)
    {
        final DilogUtil util = new DilogUtil(context);
        if (isNetworkAvailable(context))
        {
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait");
            pDialog.show();
            Gson gson = new GsonBuilder().create();
            String json = gson.toJson(registerDTO);
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    RequestConstants.SIGNUP_URL, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());
                            pDialog.dismiss();
                            //User json from json response
//                            JSONObject jsonUser = response.optJSONObject("user");
                            try {

                                ServiceResponseDTO serviceResponseDTO=new ServiceResponseDTO();
                                serviceResponseDTO.setStatusMsg(response.getString("Success"));
                                signUpView.handleSignUpResponce(serviceResponseDTO);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    // hide the progress dialog
                    pDialog.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if (response != null && response.data != null) {
                        String json = new String(response.data);
                        try {
                            JSONObject object = new JSONObject(json);
                            String message = object.optString("error");
                            if (message == null || TextUtils.isEmpty(message)) {
                                message = object.optString("message");
                            }
                            if (message == null || TextUtils.isEmpty(message)) {
                                message = object.optString("status");
                            }
                            util.showDialog(message, context);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        util.showDialog(context.getResources().getString(R.string.notAbleToLoginTrylater),context);
                    }
                }
            });
            //Register API calling
            HTFAppilication.get(context).addToRequestQueue(jsonObjReq);
        }
        else
        {
            util.showDialog(context.getResources().getString(R.string.pleaseCheckInternet), context);
        }
    }

    public void ForgotAPICall(final ILoginView loginview, String sEmail)
    {
        final DilogUtil util = new DilogUtil(context);
        if (isNetworkAvailable(context))
        {
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait");
            pDialog.show();

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("email",sEmail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    RequestConstants.FORGOT_PASSWORD_URL, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());
                            pDialog.dismiss();
                            //User json from json response
//                            JSONObject jsonUser = response.optJSONObject("user");
                            try {

                                ServiceResponseDTO serviceResponseDTO=new ServiceResponseDTO();
                                serviceResponseDTO.setStatusMsg(response.getString("message"));
                                loginview.handleForgetResponce(serviceResponseDTO);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    // hide the progress dialog
                    pDialog.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if (response != null && response.data != null) {
                        String json = new String(response.data);
                        try {
                            JSONObject object = new JSONObject(json);
                            String message = object.optString("error");
                            if (message == null || TextUtils.isEmpty(message)) {
                                message = object.optString("message");
                            }
                            if (message == null || TextUtils.isEmpty(message)) {
                                message = object.optString("status");
                            }
                            util.showDialog(message, context);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        util.showDialog(context.getResources().getString(R.string.notAbleToLoginTrylater),context);
                    }
                }
            });
            //Register API calling
            HTFAppilication.get(context).addToRequestQueue(jsonObjReq);
        }
        else
        {
            util.showDialog(context.getResources().getString(R.string.pleaseCheckInternet), context);
        }
    }
    public void ChangeAPICall(final IChangeView changeView,RegisterDTO registerDTO)
    {
        final DilogUtil util = new DilogUtil(context);
        if (isNetworkAvailable(context))
        {
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait");
            pDialog.show();

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("email",registerDTO.getEmail());
                jsonObject.put("password",registerDTO.getRePassword());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    RequestConstants.FORGOT_PASSWORD_URL, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());
                            pDialog.dismiss();
                            //User json from json response
//                            JSONObject jsonUser = response.optJSONObject("user");
                            try {

                                ServiceResponseDTO serviceResponseDTO=new ServiceResponseDTO();
                                serviceResponseDTO.setStatusMsg(response.getString("message"));
                                changeView.handleChangePassword(serviceResponseDTO);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    // hide the progress dialog
                    pDialog.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if (response != null && response.data != null) {
                        String json = new String(response.data);
                        try {
                            JSONObject object = new JSONObject(json);
                            String message = object.optString("error");
                            if (message == null || TextUtils.isEmpty(message)) {
                                message = object.optString("message");
                            }
                            if (message == null || TextUtils.isEmpty(message)) {
                                message = object.optString("status");
                            }
                            util.showDialog(message, context);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        util.showDialog(context.getResources().getString(R.string.notAbleToLoginTrylater),context);
                    }
                }
            });
            //Register API calling
            HTFAppilication.get(context).addToRequestQueue(jsonObjReq);
        }
        else
        {
            util.showDialog(context.getResources().getString(R.string.pleaseCheckInternet), context);
        }
    }

    public void getInstutes(final InstutionsView instutionsView) {
        final DilogUtil util = new DilogUtil(context);
        if (isNetworkAvailable(context))
        {
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait");
            pDialog.show();


            JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, RequestConstants.INSTUTATES_URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d(TAG, response.toString());
                            ArrayList<InstitutesDTO> instutesDTOs = new ArrayList<>();
                            if (response.length() > 0)
                            {
//                               ArrayList<InstitutesDTO> instutesDTOs = new ArrayList<>();
                                pDialog.dismiss();
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        InstitutesDTO institutesDTO=new Gson().fromJson(response.getJSONObject(i).toString(),InstitutesDTO.class);
                                        instutesDTOs.add(institutesDTO);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                ServiceResponseDTO serviceResponseDTO=new ServiceResponseDTO();
                                serviceResponseDTO.setResponse(instutesDTOs);
                                instutionsView.handleInstutesResponce(serviceResponseDTO);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            });
            //getMake List  API calling
            HTFAppilication.get(context).addToRequestQueue(arrayRequest);
        }
        else
        {
            util.showDialog(context.getResources().getString(R.string.pleaseCheckInternet), context);
        }
    }

    public void Logout() {
        final DilogUtil util = new DilogUtil(context);
        if (isNetworkAvailable(context))
        {
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait");
            pDialog.show();


            JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, RequestConstants.LOGOUT, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d(TAG, response.toString());
                            if (response.length() > 0)
                            {
//                               ArrayList<InstitutesDTO> instutesDTOs = new ArrayList<>();
                                pDialog.dismiss();
                                try {
                                    CLRealmHelper.getInstance().removeUser();
                                    Toast.makeText(context,response.getJSONObject(0).getString("success"),Toast.LENGTH_LONG).show();
                                    Intent clIntent=new Intent(context, LoginActivity.class);
                                    context.startActivity(clIntent);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    CLRealmHelper.getInstance().removeUser();
                    Toast.makeText(context,error.getMessage(),Toast.LENGTH_LONG).show();
                    ((Activity)context).finish();
                    Intent clIntent=new Intent(context, LoginActivity.class);
                    context.startActivity(clIntent);
                }
            });
            //getMake List  API calling
            HTFAppilication.get(context).addToRequestQueue(arrayRequest);
        }
        else
        {
            util.showDialog(context.getResources().getString(R.string.pleaseCheckInternet), context);
        }
    }
    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}




