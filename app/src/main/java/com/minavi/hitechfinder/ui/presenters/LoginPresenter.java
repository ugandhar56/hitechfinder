package com.minavi.hitechfinder.ui.presenters;

import com.minavi.hitechfinder.ui.base.BasePresenter;
import com.minavi.hitechfinder.ui.comp.CLSubscriber;
import com.minavi.hitechfinder.ui.view.model.LoginDTO;
import com.minavi.hitechfinder.ui.view.model.ServiceResponseDTO;
import com.minavi.hitechfinder.ui.data.remote.service.ILoginService;
import com.minavi.hitechfinder.ui.data.remote.service.LoginService;
import com.minavi.hitechfinder.ui.injection.ConfigPersistent;
import com.minavi.hitechfinder.ui.view.callbacks.ILoginView;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by yugandhar on 6/13/2017.
 */
@ConfigPersistent
public class LoginPresenter extends BasePresenter<ILoginView>
{

    @Inject
    public LoginPresenter() {
    }
    @Override
    public void attachView(ILoginView clView) {

        super.attachView(clView);
    }

    public void doLogin(LoginDTO loginDTO)
    {


        ILoginService iLoginService=new LoginService();


        Observable clObservable = iLoginService.authenticateUser(loginDTO);
        clObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CLSubscriber<ServiceResponseDTO>(getMvpView()) {
                    @Override
                    public void onNext(ServiceResponseDTO serviceResponseDTO) {
                        getMvpView().handleLoginResponce(serviceResponseDTO);
                    }



                });

    }





    @Override
    public void detachView() {
        super.detachView();
        // if (mSubscription != null) mSubscription.unsubscribe();
    }


}
