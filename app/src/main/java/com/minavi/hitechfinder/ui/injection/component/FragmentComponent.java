package com.minavi.hitechfinder.ui.injection.component;

import com.minavi.hitechfinder.ui.injection.PerFragment;
import com.minavi.hitechfinder.ui.injection.module.FragmentModule;
import com.minavi.hitechfinder.ui.view.LoginFragment;

import dagger.Subcomponent;

/**
 * Created by yugandhar on 6/16/2017.
 */
@PerFragment
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent
{
    void inject(LoginFragment loginFragment);
}
