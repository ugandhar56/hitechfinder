package com.minavi.hitechfinder.ui.data.remote.net.provider;


import com.minavi.hitechfinder.ui.data.remote.net.dto.CLRequestDTO;
import com.minavi.hitechfinder.ui.data.remote.net.dto.CLResponseDTO;
import com.minavi.hitechfinder.ui.data.remote.net.request.IRequest;
import com.minavi.hitechfinder.ui.data.remote.net.response.IResponse;

import okhttp3.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * Created by yugandhar on 10-11-2016.
 */
public class OkHttpProvider implements IHttpProvider
{
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");



    private Request buildRequest(CLRequestDTO clRequestDTO) throws Exception
    {
        Request request=null;


        String sURL=clRequestDTO.getURL();
        Map<String,String> clMapParams=clRequestDTO.getParams();
        byte byRequestMethod=clRequestDTO.getRequestMethod();
        byte byContentType=clRequestDTO.getContentType();



        Request.Builder clRequestBuilder=new Request.Builder();
        HttpUrl.Builder clURLBuilder=HttpUrl.parse(sURL).newBuilder();
        RequestBody clRequestBody = null;

        if(byContentType== IRequest.CONTENT_JSON)
            clRequestBody = RequestBody.create(JSON, clRequestDTO.getJsonString());
        else if(byContentType==IRequest.CONTENT_FORM || byContentType==IRequest.CONTENT_MULTIPART)
        {
            if (clMapParams != null)
            {
                FormBody.Builder clFormBuilder=null;
                MultipartBody.Builder clMultiPartBuilder=null;

                if(byContentType==IRequest.CONTENT_MULTIPART)
                    clMultiPartBuilder=new MultipartBody.Builder();
                else if (byRequestMethod == IRequest.METHOD_POST)
                    clFormBuilder = new FormBody.Builder();


                Iterator clParams = clMapParams.keySet().iterator();
                String sName,sValue;

                while (clParams.hasNext())
                {
                    sName = (String) clParams.next();
                    sValue = clMapParams.get(sName);

                    if(byContentType==IRequest.CONTENT_MULTIPART)
                        clMultiPartBuilder.addFormDataPart(sName, sValue);
                    else if (byRequestMethod == IRequest.METHOD_GET)
                        clURLBuilder.addQueryParameter(sName, sValue);
                    else if (byRequestMethod == IRequest.METHOD_POST)
                        clFormBuilder.add(sName, sValue);

                    sURL = clURLBuilder.build().toString();
                }

                if(clFormBuilder!=null)
                    clRequestBody=clFormBuilder.build();

            }
        }

       /* else if(byContentType==IRequest.CONTENT_MULTIPART)
        {
            MultipartBody.Builder clMultiPartBuilder=new MultipartBody.Builder();
            clMultiPartBuilder.setType(MultipartBody.FORM);

            *//*.addFormDataPart("title", "Square Logo")
            .addFormDataPart("image", "logo-square.png",
                    RequestBody.create(MEDIA_TYPE_PNG, new File("website/static/logo-square.png")))*//*

            clRequestBody=clMultiPartBuilder.build();
        }*/

        clRequestBuilder.url(sURL);

        if(clRequestBody!=null)
            clRequestBuilder.post(clRequestBody); //RequestBody requestBody=clFormBuilder.build();


//        UserDTO clUserDTO= CLRealmHelper.getInstance().getUserDetails()/*CLDataManager.getAppBuffer().getUserDTO()*/;
//        UserDTO clUserDTO=CLDataManager.getAppBuffer().getUserDTO();
/*
        if(clUserDTO!=null)
            clRequestBuilder.addHeader("Cookie","JSESSIONID="+clUserDTO.getSessionId());
        request=clRequestBuilder.build();*/
//            response=client.newCall(request).execute();


        /*client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                }
                else
                {
                    clResponseListener.onResponse(IRequest.STATUS_SUCCESS,response);
                }
            }

            });*/


        return request;
    }


    public CLResponseDTO executeRequest(CLRequestDTO clRequestDTO) throws RuntimeException
    {
        CLResponseDTO clResponseDTO=new CLResponseDTO();

        Response response=null;
        int iResponseCode =0;

        try
        {
            OkHttpClient.Builder clHttpClientBuilder=new OkHttpClient.Builder();
            clHttpClientBuilder.connectTimeout(10, TimeUnit.SECONDS);
            clHttpClientBuilder.writeTimeout(10, TimeUnit.SECONDS);
            clHttpClientBuilder.readTimeout(30, TimeUnit.SECONDS);

            OkHttpClient client = clHttpClientBuilder.build();


            Request request=buildRequest(clRequestDTO);
            Call clCall=client.newCall(request);
            response=clCall.execute();

            iResponseCode = response.code();

            if(iResponseCode==HttpURLConnection.HTTP_OK) {

                clResponseDTO.setStatus(IResponse.STATUS_SUCCESS);
                clResponseDTO.setResponse(response.body().string());
            }
           else
            {
                //throw new NetException(getHttpStatusMsg(iResponseCode)+","+response.message(),iResponseCode);
                throw new RuntimeException();
                //response.message();
            }
        }
        catch (RuntimeException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new RuntimeException();
        }

        return clResponseDTO;
    }


    public void enqueueRequest(CLRequestDTO clRequestDTO,final IResponse.Listener clResponseListener)
    {
        try {


            OkHttpClient.Builder clHttpClientBuilder = new OkHttpClient.Builder();
            clHttpClientBuilder.connectTimeout(10, TimeUnit.SECONDS);
            clHttpClientBuilder.writeTimeout(10, TimeUnit.SECONDS);
            clHttpClientBuilder.readTimeout(30, TimeUnit.SECONDS);

            Request request = buildRequest(clRequestDTO);

            OkHttpClient client = clHttpClientBuilder.build();

            client.newCall(request).enqueue(new Callback() {

                @Override
                public void onFailure(Call call, IOException e)
                {

                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        throw new IOException("Unexpected code " + response);
                    } else {
                        clResponseListener.onResponse(IResponse.STATUS_SUCCESS, response);
                    }
                }

            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private String getHttpStatusMsg(int iStatus)
    {
        String sMsg=null;
        if(iStatus == HttpURLConnection.HTTP_INTERNAL_ERROR)
            sMsg = "Internal server error : "+iStatus;
        else if(iStatus == HttpURLConnection.HTTP_GATEWAY_TIMEOUT)
            sMsg = "Gateway timeout : "+iStatus;
        else if(iStatus == HttpURLConnection.HTTP_NOT_FOUND)
            sMsg = "Requested resource not found : "+iStatus;
        else if(iStatus == HttpURLConnection.HTTP_CLIENT_TIMEOUT)
            sMsg = "Request timeout : "+iStatus;
        else
            sMsg = "Server error : "+iStatus;

        return sMsg;
    }



}
